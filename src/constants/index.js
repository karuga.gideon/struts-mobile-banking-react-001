export const SET_USER = "SET_USER";
export const SET_PAPER = "SET_PAPER";
export const ACCEPT_TERMS = "ACCEPT_TERMS";
export const WEB_BASE_URL = "http://176.58.124.136:8092/Mobile Banking";
export const API_BASE_URL_LIVE = "http://176.58.124.136:8092/Mobile Banking";
export const API_BASE_URL = "http://localhost:9010/esdws";
export const PROFILE_PAYLOAD = "PROFILE_PAYLOAD";
export const SURVEY_DETAILS = "SURVEY_DETAILS";
export const TOGGLE_LOADER = "TOGGLE_LOADER";
export const VERIFICATION_ERROR_CODE = "ER0002";
export const SUCCESS = "Success";
export const VERIFICATION = "Verification";
