import React from "react";
import 'react-widgets/dist/css/react-widgets.css';
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";
import { FaUsers, FaBuilding, FaAddressCard, FaTable, FaFacebook, FaTwitter, FaLinkedin, FaGooglePlus } from 'react-icons/fa';

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import { getUserDetails, isLoggedIn } from "../../../util/AuthService";

import { faHome, faLaptop, faMoon, faBell, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Dashboard";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  
  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
        
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    axios({
      url: API_BASE_URL + "/reports/summary",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));        
        if (!response.data.error) {
          this.setState({ records: json_data });
          this.setState({ graphArray: json_data });
          this.setState({
            collections_count: json_data.collections_count,
            clients_count: json_data.clients_count,
            sales_representatives_count: json_data.sales_representatives_count,
            users_count: json_data.users_count
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  render() {

    const { collections_count, clients_count, sales_representatives_count, users_count } = this.state;

    return (
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
        
        { loadHeader() }

    <div className="app-body">
      
      { loadSidebarMenu() }

      <main className="main">
        {/* <!-- Breadcrumb--> */}
        <ol className="breadcrumb">
          <li className="breadcrumb-item">Home</li>
          <li className="breadcrumb-item">
            <a href="dashboard">Admin</a>
          </li>
          <li className="breadcrumb-item active">Dashboard</li>
          {/* <!-- Breadcrumb Menu--> */}
          <li className="breadcrumb-menu d-md-down-none">
            <div className="btn-group" role="group" aria-label="Button group">
              <a className="btn" href="dashboard">
                <i className="icon-speech"></i>
              </a>
              <a className="btn" href="dashboard">
                <i className="icon-graph"></i>  Dashboard</a>
              <a className="btn" href="/">
                <i className="icon-settings"></i> Logout</a>
            </div>
          </li>
        </ol>
        <div className="container-fluid">
          <div className="animated fadeIn">

          <div className="row">
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-1 clearfix">
                  {/* <i className="fas fa-cogs bg-primary p-1 font-2xl mr-1 float-left"></i> */}
                  <FontAwesomeIcon icon={faHome} className="bg-primary p-1 font-2xl mr-1 float-left" />
                  <div className="h5 text-primary mb-0 mt-h">$1.999,50</div>
                  <div className="text-muted text-uppercase font-weight-bold font-xs">Income</div>
                </div>
                <div className="card-footer p-x-1 py-h">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View More 
                    {/* <i className="fa fa-angle-right float-right font-lg"></i> */}
                    <FontAwesomeIcon icon={faAngleRight} className="float-right font-lg" />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-1 clearfix">
                  {/* <i className="fa fa-laptop bg-info p-1 font-2xl mr-1 float-left"></i> */}
                  <FontAwesomeIcon icon={faLaptop} className="bg-info p-1 font-2xl mr-1 float-left" />
                  <div className="h5 text-info mb-0 mt-h">$1.999,50</div>
                  <div className="text-muted text-uppercase font-weight-bold font-xs">Income</div>
                </div>
                <div className="card-footer p-x-1 py-h">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View More 
                    {/* <i className="fa fa-angle-right float-right font-lg"></i> */}
                    <FontAwesomeIcon icon={faAngleRight} className="float-right font-lg" />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-1 clearfix">
                  {/* <i className="fa fa-moon-o bg-warning p-1 font-2xl mr-1 float-left"></i> */}
                  <FontAwesomeIcon icon={faMoon} className="bg-warning p-1 font-2xl mr-1 float-left" />
                  <div className="h5 text-warning mb-0 mt-h">$1.999,50</div>
                  <div className="text-muted text-uppercase font-weight-bold font-xs">Income</div>
                </div>
                <div className="card-footer p-x-1 py-h">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View More 
                    {/* <i className="fa fa-angle-right float-right font-lg"></i> */}
                    <FontAwesomeIcon icon={faAngleRight} className="float-right font-lg" />
                  </a>
                </div>
              </div>
            </div>
            <div className="col-6 col-lg-3">
              <div className="card">
                <div className="card-body p-1 clearfix">
                  {/* <i className="fa fa-bell bg-danger p-1 font-2xl mr-1 float-left"></i> */}
                  <FontAwesomeIcon icon={faBell} className="bg-danger p-1 font-2xl mr-1 float-left" />
                  <div className="h5 text-danger mb-0 mt-h">$1.999,50</div>
                  <div className="text-muted text-uppercase font-weight-bold font-xs">Income</div>
                </div>
                <div className="card-footer p-x-1 py-h">
                  <a className="font-weight-bold font-xs btn-block text-muted" href="#">View More 
                    {/* <i className="fa fa-angle-right float-right font-lg"></i> */}
                    <FontAwesomeIcon icon={faAngleRight} className="float-right font-lg" />
                  </a>
                </div>
              </div>
            </div>
          </div>

            <div className="row">
              <div className="col-sm-6 col-lg-3">
                <div className="card text-white bg-primary">
                  <div className="card-body pb-0">
                    <div className="btn-group float-right">
                      <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="icon-settings"><FaTable /></i>                        
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a className="dropdown-item" href="dashboard">Action</a>
                        <a className="dropdown-item" href="dashboard">Another action</a>
                        <a className="dropdown-item" href="dashboard">Something else here</a>
                      </div>
                    </div>
                    <div className="text-value">786,656</div>
                    <div>Transactions</div>
                  </div>
                  <div className="chart-wrapper mt-3 mx-3" style={{ height:"70px" }}>
                    <canvas className="chart" id="card-chart1" height="70"></canvas>
                    <canvas class="chart chart-line ng-isolate-scope chartjs-render-monitor" chart-data="data" chart-labels="labels" 
                      chart-legend="false" chart-series="series" chart-options="options" chart-colors="colors" height="87" 
                        style={{ display: "block", height: "70px", width: "259px",  width: "323" }}></canvas>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="card text-white bg-info">
                  <div className="card-body pb-0">
                    <button className="btn btn-transparent p-0 float-right" type="button">
                      <i className="icon-location-pin"><FaBuilding /></i>
                    </button>
                    <div className="text-value">2,200</div>
                    <div>Institutions</div>
                  </div>
                  <div className="chart-wrapper mt-3 mx-3" style={{ height:"70px" }}>
                    <canvas className="chart" id="card-chart2" height="70"></canvas>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="card text-white bg-warning">
                  <div className="card-body pb-0">
                    <div className="btn-group float-right">
                      <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="icon-settings"><FaAddressCard /></i>
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a className="dropdown-item" href="dashboard">Action</a>
                        <a className="dropdown-item" href="dashboard">Another action</a>
                        <a className="dropdown-item" href="dashboard">Something else here</a>
                      </div>
                    </div>
                    <div className="text-value">60,000</div>
                    <div>Students</div>
                  </div>
                  <div className="chart-wrapper mt-3" style={{height:"70px" }}>
                    <canvas className="chart" id="card-chart3" height="70"></canvas>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="card text-white bg-danger">
                  <div className="card-body pb-0">
                    <div className="btn-group float-right">
                      <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="icon-settings"><FaUsers /></i>
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a className="dropdown-item" href="dashboard">Action</a>
                        <a className="dropdown-item" href="dashboard">Another action</a>
                        <a className="dropdown-item" href="dashboard">Something else here</a>
                      </div>
                    </div>
                    <div className="text-value">2,250</div>
                    <div>Users</div>
                  </div>
                  <div className="chart-wrapper mt-3 mx-3" style={{height:"70px" }}>
                    <canvas className="chart" id="card-chart4" height="70"></canvas>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
            </div>
            {/* <!-- /.row--> */}
            
            {/* <!-- /.card--> */}
            <div className="row">
              <div className="col-sm-6 col-lg-3">
                <div className="brand-card">
                  <div className="brand-card-header bg-facebook">
                    {/* <i className="fa fa-facebook"></i> */}
                    <i className="icon-settings"><FaFacebook /></i>    
                    <div className="chart-wrapper">
                      <canvas id="social-box-chart-1" height="90"></canvas>
                    </div>
                  </div>
                  <div className="brand-card-body">
                    <div>
                      <div className="text-value">89k</div>
                      <div className="text-uppercase text-muted small">friends</div>
                    </div>
                    <div>
                      <div className="text-value">459</div>
                      <div className="text-uppercase text-muted small">feeds</div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="brand-card">
                  <div className="brand-card-header bg-twitter">
                    {/* <i className="fa fa-twitter"></i> */}
                    <i className="icon-settings"><FaTwitter /></i>    
                    <div className="chart-wrapper">
                      <canvas id="social-box-chart-2" height="90"></canvas>
                    </div>
                  </div>
                  <div className="brand-card-body">
                    <div>
                      <div className="text-value">973k</div>
                      <div className="text-uppercase text-muted small">followers</div>
                    </div>
                    <div>
                      <div className="text-value">1.792</div>
                      <div className="text-uppercase text-muted small">tweets</div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="brand-card">
                  <div className="brand-card-header bg-linkedin">
                    <i className="icon-settings"><FaLinkedin /></i>    
                    <div className="chart-wrapper">
                      <canvas id="social-box-chart-3" height="90"></canvas>
                    </div>
                  </div>
                  <div className="brand-card-body">
                    <div>
                      <div className="text-value">500+</div>
                      <div className="text-uppercase text-muted small">contacts</div>
                    </div>
                    <div>
                      <div className="text-value">292</div>
                      <div className="text-uppercase text-muted small">feeds</div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
              <div className="col-sm-6 col-lg-3">
                <div className="brand-card">
                  <div className="brand-card-header bg-google-plus">
                    <i className="icon-settings"><FaGooglePlus /></i>    
                    <div className="chart-wrapper">
                      <canvas id="social-box-chart-4" height="90"></canvas>
                    </div>
                  </div>
                  <div className="brand-card-body">
                    <div>
                      <div className="text-value">894</div>
                      <div className="text-uppercase text-muted small">followers</div>
                    </div>
                    <div>
                      <div className="text-value">92</div>
                      <div className="text-uppercase text-muted small">circles</div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
            </div>

            <div className="row">
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">89.9%</div>
                    <div>Lorem ipsum...</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-success" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Lorem ipsum dolor sit amet enim.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">12.124</div>
                    <div>Lorem ipsum...</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Lorem ipsum dolor sit amet enim.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">$98.111,00</div>
                    <div>Lorem ipsum...</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-warning" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Lorem ipsum dolor sit amet enim.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">2 TB</div>
                    <div>Lorem ipsum...</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Lorem ipsum dolor sit amet enim.</small>
                  </div>
                </div>
              </div>
            </div>

          <div className="row">
             <div className="col-md-12">          
              <div className="card-group">
                  <div className="card">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-2">
                        <i className="icon-people"></i>
                      </div>
                      <div className="h4 mb-0">87.500</div>
                      <small className="text-muted text-uppercase font-weight-bold">Visitors</small>
                      <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%",  ariaValuenow: "25",  ariaValuemin: "0", ariaValuemax : "100" }}></div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-2">
                        <i className="icon-user-follow"></i>
                      </div>
                      <div className="h4 mb-0">385</div>
                      <small className="text-muted text-uppercase font-weight-bold">New Clients</small>
                      <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar bg-success" role="progressbar" style={{ width: "25%",  ariaValueNow: "25",  ariaValuemin: "0", ariaValuemax : "100" }}></div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-2">
                        <i className="icon-basket-loaded"></i>
                      </div>
                      <div className="h4 mb-0">1238</div>
                      <small className="text-muted text-uppercase font-weight-bold">Products sold</small>
                      <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: "25%",  ariaValueNow: "25",  ariaValuemin: "0", ariaValuemax : "100" }}></div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-2">
                        <i className="icon-pie-chart"></i>
                      </div>
                      <div className="h4 mb-0">28%</div>
                      <small className="text-muted text-uppercase font-weight-bold">Returning Visitors</small>
                      <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar" role="progressbar" style={{ width: "25%",  ariaValueNow: "25",  ariaValuemin: "0", ariaValuemax : "100" }}></div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-2">
                        <i className="icon-speedometer"></i>
                      </div>
                      <div className="h4 mb-0">5:34:11</div>
                      <small className="text-muted text-uppercase font-weight-bold">Avg. Time</small>
                      <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%",  ariaValueNow: "25",  ariaValuemin: "0", ariaValuemax : "100" }}></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
            </div>

            <br/>

            {/* <!-- /.row--> */}
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-header">Traffic & Sales</div>
                  <div className="card-body">
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="row">
                          <div className="col-sm-6">
                            <div className="callout callout-info">
                              <small className="text-muted">New Clients</small>
                              <br />
                              <strong className="h4">9,123</strong>
                              <div className="chart-wrapper">
                                <canvas id="sparkline-chart-1" width="100" height="30"></canvas>
                              </div>
                            </div>
                          </div>
                          {/* <!-- /.col--> */}
                          <div className="col-sm-6">
                            <div className="callout callout-danger">
                              <small className="text-muted">Recuring Clients</small>
                              <br />
                              <strong className="h4">22,643</strong>
                              <div className="chart-wrapper">
                                <canvas id="sparkline-chart-2" width="100" height="30"></canvas>
                              </div>
                            </div>
                          </div>
                          {/* <!-- /.col--> */}
                        </div>
                        {/* <!-- /.row--> */}
                        <hr className="mt-0" />
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Monday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "34%"}} aria-valuenow="34" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "78%"}} aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Tuesday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "56%"}} aria-valuenow="56" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "94%"}} aria-valuenow="94" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Wednesday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "12%"}} aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "67%"}} aria-valuenow="67" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Thursday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "43%"}} aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "91%"}} aria-valuenow="91" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Friday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "22%"}} aria-valuenow="22" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "73%"}} aria-valuenow="73" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Saturday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "53%"}} aria-valuenow="53" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "82%"}} aria-valuenow="82" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-4">
                          <div className="progress-group-prepend">
                            <span className="progress-group-text">Sunday</span>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-info" role="progressbar" style={{width: "9%" }}aria-valuenow="9" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-danger" role="progressbar" style={{width: "69%"}} aria-valuenow="69" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <!-- /.col--> */}
                      <div className="col-sm-6">
                        <div className="row">
                          <div className="col-sm-6">
                            <div className="callout callout-warning">
                              <small className="text-muted">Pageviews</small>
                              <br/>
                              <strong className="h4">78,623</strong>
                              <div className="chart-wrapper">
                                <canvas id="sparkline-chart-3" width="100" height="30"></canvas>
                              </div>
                            </div>
                          </div>
                          {/* <!-- /.col--> */}
                          <div className="col-sm-6">
                            <div className="callout callout-success">
                              <small className="text-muted">Organic</small>
                              <br/>
                              <strong className="h4">49,123</strong>
                              <div className="chart-wrapper">
                                <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                              </div>
                            </div>
                          </div>
                          {/* <!-- /.col--> */}
                        </div>
                        {/* <!-- /.row--> */}
                        <hr className="mt-0" />
                        <div className="progress-group">
                          <div className="progress-group-header">
                            <i className="icon-user progress-group-icon"></i>
                            <div>Male</div>
                            <div className="ml-auto font-weight-bold">43%</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-warning" role="progressbar" style={{ width: "43%"}}  aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group mb-5">
                          <div className="progress-group-header">
                            <i className="icon-user-female progress-group-icon"></i>
                            <div>Female</div>
                            <div className="ml-auto font-weight-bold">37%</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-warning" role="progressbar" style={{ width: "43%"}}  aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group">
                          <div className="progress-group-header align-items-end">
                            <i className="icon-globe progress-group-icon"></i>
                            <div>Organic Search</div>
                            <div className="ml-auto font-weight-bold mr-2">191.235</div>
                            <div className="text-muted small">(56%)</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-success" role="progressbar" style={{ width: "56%"}}  aria-valuenow="56" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group">
                          <div className="progress-group-header align-items-end">
                            <i className="icon-social-facebook progress-group-icon"></i>
                            <div>Facebook</div>
                            <div className="ml-auto font-weight-bold mr-2">51.223</div>
                            <div className="text-muted small">(15%)</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-success" role="progressbar" style={{ width: "15%"}}  aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group">
                          <div className="progress-group-header align-items-end">
                            <i className="icon-social-twitter progress-group-icon"></i>
                            <div>Twitter</div>
                            <div className="ml-auto font-weight-bold mr-2">37.564</div>
                            <div className="text-muted small">(11%)</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-success" role="progressbar" style={{ width: "11%"}}  aria-valuenow="11" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div className="progress-group">
                          <div className="progress-group-header align-items-end">
                            <i className="icon-social-linkedin progress-group-icon"></i>
                            <div>LinkedIn</div>
                            <div className="ml-auto font-weight-bold mr-2">27.319</div>
                            <div className="text-muted small">(8%)</div>
                          </div>
                          <div className="progress-group-bars">
                            <div className="progress progress-xs">
                              <div className="progress-bar bg-success" role="progressbar" style={{ width: "8%" }} aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <!-- /.col--> */}
                    </div>
                    {/* <!-- /.row--> */}
                    <br/>
                    
                  </div>
                </div>
              </div>
              {/* <!-- /.col--> */}
            </div>
            {/* <!-- /.row--> */}
          </div>
        </div>
      </main>
      <aside className="aside-menu">
        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" data-toggle="tab" href="#timeline" role="tab">
              <i className="icon-list"></i>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-toggle="tab" href="#messages" role="tab">
              <i className="icon-speech"></i>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-toggle="tab" href="#settings" role="tab">
              <i className="icon-settings"></i>
            </a>
          </li>
        </ul>
        {/* <!-- Tab panes--> */}
        <div className="tab-content">
          <div className="tab-pane active" id="timeline" role="tabpanel">
            <div className="list-group list-group-accent">
              <div className="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Today</div>
              <div className="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div className="avatar float-right">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                </div>
                <div>Meeting with
                  <strong>Lucas</strong>
                </div>
                <small className="text-muted mr-3">
                  <i className="icon-calendar"></i>  1 - 3pm</small>
                <small className="text-muted">
                  <i className="icon-location-pin"></i>  Palo Alto, CA</small>
              </div>
              <div className="list-group-item list-group-item-accent-info">
                <div className="avatar float-right">
                  <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                </div>
                <div>Skype with
                  <strong>Megan</strong>
                </div>
                <small className="text-muted mr-3">
                  <i className="icon-calendar"></i>  4 - 5pm</small>
                <small className="text-muted">
                  <i className="icon-social-skype"></i>  On-line</small>
              </div>
              <div className="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Tomorrow</div>
              <div className="list-group-item list-group-item-accent-danger list-group-item-divider">
                <div>New UI Project -
                  <strong>deadline</strong>
                </div>
                <small className="text-muted mr-3">
                  <i className="icon-calendar"></i>  10 - 11pm</small>
                <small className="text-muted">
                  <i className="icon-home"></i>  creativeLabs HQ</small>
                <div className="avatars-stack mt-2">
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/2.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/3.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                </div>
              </div>
              <div className="list-group-item list-group-item-accent-success list-group-item-divider">
                <div>
                  <strong>#10 Startups.Garden</strong> Meetup</div>
                <small className="text-muted mr-3">
                  <i className="icon-calendar"></i>  1 - 3pm</small>
                <small className="text-muted">
                  <i className="icon-location-pin"></i>  Palo Alto, CA</small>
              </div>
              <div className="list-group-item list-group-item-accent-primary list-group-item-divider">
                <div>
                  <strong>Team meeting</strong>
                </div>
                <small className="text-muted mr-3">
                  <i className="icon-calendar"></i>  4 - 6pm</small>
                <small className="text-muted">
                  <i className="icon-home"></i>  creativeLabs HQ</small>
                <div className="avatars-stack mt-2">
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/2.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/3.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                  <div className="avatar avatar-xs">
                    <img className="img-avatar" src="img/avatars/8.jpg" alt="admin@bootstrapmaster.com" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="tab-pane p-3" id="messages" role="tabpanel">
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Lukasz Holeczek</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Lukasz Holeczek</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Lukasz Holeczek</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Lukasz Holeczek</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Lukasz Holeczek</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
          </div>
          <div className="tab-pane p-3" id="settings" role="tabpanel">
            <h6>Settings</h6>
            <div className="aside-options">
              <div className="clearfix mt-4">
                <small>
                  <b>Option 1</b>
                </small>
                <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                  <input className="switch-input" type="checkbox" checked="" />
                  <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                </label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
              </div>
            </div>
            <div className="aside-options">
              <div className="clearfix mt-3">
                <small>
                  <b>Option 2</b>
                </small>
                <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                  <input className="switch-input" type="checkbox" />
                  <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                </label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
              </div>
            </div>
            <div className="aside-options">
              <div className="clearfix mt-3">
                <small>
                  <b>Option 3</b>
                </small>
                <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                  <input className="switch-input" type="checkbox" />
                  <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                </label>
              </div>
            </div>
            <div className="aside-options">
              <div className="clearfix mt-3">
                <small>
                  <b>Option 4</b>
                </small>
                <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                  <input className="switch-input" type="checkbox" checked="" />
                  <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                </label>
              </div>
            </div>
            <hr/>
            <h6>System Utilization</h6>
            <div className="text-uppercase mb-1 mt-4">
              <small>
                <b>CPU Usage</b>
              </small>
            </div>
            <div className="progress progress-xs">
              <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small className="text-muted">348 Processes. 1/4 Cores.</small>
            <div className="text-uppercase mb-1 mt-2">
              <small>
                <b>Memory Usage</b>
              </small>
            </div>
            <div className="progress progress-xs">
              <div className="progress-bar bg-warning" role="progressbar" style={{ width: "70%" }} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small className="text-muted">11444GB/16384MB</small>
            <div className="text-uppercase mb-1 mt-2">
              <small>
                <b>SSD 1 Usage</b>
              </small>
            </div>
            <div className="progress progress-xs">
              <div className="progress-bar bg-danger" role="progressbar" style={{ width: "95%" }} aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small className="text-muted">243GB/256GB</small>
            <div className="text-uppercase mb-1 mt-2">
              <small>
                <b>SSD 2 Usage</b>
              </small>
            </div>
            <div className="progress progress-xs">
              <div className="progress-bar bg-success" role="progressbar" style={{ width: "10%" }} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small className="text-muted">25GB/256GB</small>
          </div>
        </div>
      </aside>
    </div>
    <footer className="app-footer">
      <div>
        <a href="https://coreui.io">CoreUI</a>
        <span>&copy; 2018 creativeLabs.</span>
      </div>
      <div className="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io">CoreUI</a>
      </div>
    </footer>    
  </div>
    );
  }
}

export default Dashboard;
