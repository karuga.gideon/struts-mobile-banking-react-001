import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import { Link } from "react-router-dom";
import { Table, Form } from "semantic-ui-react";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { getUserDetails, isLoggedIn } from "../../../util/AuthService";

class Collections extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Collections";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      records: [],
      recordsSearch: [],
      recordsCount: 0, 
      graphArray: [],
      isLoading: false,
      pageOfItems: [], 
      currentPage: 1,
      recordsPerPage: 10
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(e) {
    // Variable to hold the original version of the list
    let currentList = [];
    // Variable to hold the filtered list before putting into state
    let newList = [];

    // If the search bar isn't empty
    if (e.target.value !== "") {      
        // Assign the original list to currentList
        currentList = this.state.recordsSearch;

        // Use .filter() to determine which items should be displayed
        // based on the search terms
        newList = currentList.filter(item => {
          // change current item to lowercase
          const lc = item.collection.invoice_number.toLowerCase() || item.client.name.toLowerCase();
          // change search term to lowercase
          const filter = e.target.value.toLowerCase();
          // check to see if the current list item includes the search term
          // If it does, it will be added to newList. Using lowercase eliminates
          // issues with capitalization in search terms and search content
          return lc.includes(filter);
        });

        currentList.filter(item => {
          const lc = item.client.name.toLowerCase();
          const filter = e.target.value.toLowerCase();          
          if (lc.includes(filter)){
            newList.push(item); 
          }
          return newList; 
        });
        
    } else {
      // If the search bar is empty, set newList to original task list
      newList = this.state.recordsSearch;
    }
        // Set the filtered state based on what our rules added to newList
    this.setState({
      records: newList
    });
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {
    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
        
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    axios({
      url: API_BASE_URL + "/collections",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        
        var json_data = JSON.parse(JSON.stringify(response.data));

        if (!response.data.error) {
          this.setState({ records: json_data.collections });
          this.setState({ recordsCount: json_data.collections_count });
          this.setState({ recordsSearch: json_data.collections });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  render() {

    const { records, currentPage, recordsPerPage, recordsCount } = this.state;

    // Logic for displaying current records 
    const indexOfLastRecord = currentPage * recordsPerPage;
    const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
    const currentRecords = records.slice(indexOfFirstRecord, indexOfLastRecord);


    // Logic for displaying page numbers
    const recordsPageNumbers = [];
    for (let i = 1; i <= Math.ceil(records.length / recordsPerPage); i++) {
      recordsPageNumbers.push(i);
    }

    const renderRecordsPageNumbers = recordsPageNumbers.map(number => {
      return (
        <li 
          className="field btn btn-primary btn-xl text-uppercase"
          color="green"
          style={{
            marginRight: "0.3em",                         
            backgroundColor: "#6bd098"
          }}
          key={number}
          id={number}
          onClick={this.handleClick}
        >              
           {number}
         
        </li>
      );
    });

    let searchBox = (    
    <div style={{ float:"right", clear:"both" }}>      
      <input style={{ marginRight: "10px", borderRadius: "5px", fontSize: "12pt" }} 
        type="text" className="input" placeholder="Search..." onChange={this.handleSearch} />
    </div>);

    let createCollectionButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <div style={{  }}>
      
      <Link to="create-collection">
        <Form.Field
          primary
          className="field btn btn-primary btn-sm text-uppercase"
          type="submit"
          color="green"
          style={{ backgroundColor: "#6bd098" }}
          onClick={this.onSubmit}
        >
          Add Collection 
        </Form.Field>
      </Link>
    </div>
    );

    let viewButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#6bd098" }}
        onClick={this.onSubmit}
      >
        View
      </Form.Field>
    );

    let updateButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#4acccd", marginLeft: "5px" }}
        onClick={this.onSubmit}
      >
        Update
      </Form.Field>
    );


    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />
        <div className="main-panel" ref="mainPanel">
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col xs={12}>
                <Card>
                  <CardHeader>
                    <CardTitle tag="h5">Collections ({recordsCount})  &nbsp; {searchBox} </CardTitle>                  
                  </CardHeader>
                  <CardBody style={{ overflowX: "hidden", msOverflowY: "hidden" }}>

                    <hr/>
                    {createCollectionButton}
                    <hr/>

                    <Table
                      celled
                      selectable
                      responsive
                      style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                    >
                      <Table.Header>
                        <Table.Row style={{ color: "#51cbce" }}>
                          <Table.HeaderCell>Invoice NO</Table.HeaderCell>
                          <Table.HeaderCell>Client Name</Table.HeaderCell>
                          <Table.HeaderCell>Invoice Date</Table.HeaderCell>
                          <Table.HeaderCell>Credit Period</Table.HeaderCell>
                          <Table.HeaderCell>Amount</Table.HeaderCell>
                          <Table.HeaderCell>Balance</Table.HeaderCell>
                          <Table.HeaderCell>Action</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        {currentRecords.map(record => (
                          <Table.Row>
                            <Table.Cell>{record.collection.invoice_number}</Table.Cell>
                            <Table.Cell>{record.client.name}</Table.Cell>
                            <Table.Cell>{record.collection.invoice_date}</Table.Cell>                            
                            <Table.Cell>{record.collection.credit_period}</Table.Cell>
                            <Table.Cell>{record.collection.amount}</Table.Cell>
                            <Table.Cell>{record.collection.balance}</Table.Cell>
                            <Table.Cell>
                              <Link to={`/collection-details/${record.collection.id}`}>
                                {viewButton}
                              </Link>

                              <Link to={`/collection-details/${record.collection.id}`}>
                                {updateButton}
                              </Link>
                            </Table.Cell>
                          </Table.Row>
                        ))}
                      </Table.Body>
                    </Table>

                    <ul style={{
                      listStyle: "none", 
                      display: "flex"
                    }}>
                      {renderRecordsPageNumbers}
                    </ul>
                    
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        />
      </div>
    );
  }
}

export default Collections;
