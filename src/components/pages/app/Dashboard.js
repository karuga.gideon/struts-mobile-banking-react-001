import React from "react";
import 'react-widgets/dist/css/react-widgets.css';
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import { getUserDetails, isLoggedIn } from "../../../util/AuthService";
import { AreaChart } from 'react-chartkick'; 
import { Link } from "react-router-dom";


class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Dashboard";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning", 
      invoicesChartData: [] 
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  
  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    axios({
      url: API_BASE_URL + "/reports/summary",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));

        if (!response.data.error) {
          this.setState({ records: json_data });
          this.setState({ graphArray: json_data });

          this.setState({
            all_invoices_count: json_data.all_invoices_count.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            all_invoices_vat_sum: json_data.all_invoices_vat_sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            all_invoices_total_sum: json_data.all_invoices_total_sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            invoices_failed_to_sign_count: json_data.invoices_failed_to_sign_count.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
          });
        }
      })
      .catch(err => {
        console.log(err);
      });


      // Retrieve Invoices Chart Data 
      axios({
        url: API_BASE_URL + "/dashboard/invoices/chart",
        method: "GET",
        withCredentials: false
      })
        .then(response => {
          var json_data = JSON.parse(JSON.stringify(response.data));
  
          if (!response.data.error) {
            this.setState({ invoicesChartData: json_data });
          }
        })
        .catch(err => {
          console.log(err);
        });        

  }

  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  render() {

    const { all_invoices_count, all_invoices_vat_sum, all_invoices_total_sum, 
        invoices_failed_to_sign_count, invoicesChartData } = this.state;

    var arr = {};
    
    invoicesChartData.forEach(processData); 
    function processData(record, index){
      var babyArr = {};
      babyArr[0] = record.date;
      babyArr[1] = record.records_count;
      arr[record.date] = record.records_count;
    }

    return (
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
        
        { loadHeader() }

    <div className="app-body">
      
      { loadSidebarMenu("/dashboard") }

      <main className="main">
        {/* <!-- Breadcrumb--> */}
        <ol className="breadcrumb">
          <li className="breadcrumb-item">Home</li>
          <li className="breadcrumb-item">
            <Link to="dashboard">Admin</Link>
          </li>
          <li className="breadcrumb-item active">Dashboard</li>
          {/* <!-- Breadcrumb Menu--> */}
          <li className="breadcrumb-menu d-md-down-none">
            <div className="btn-group" role="group" aria-label="Button group">
              <Link className="btn" to="dashboard">
                <i className="icon-speech"></i>
              </Link>
              <Link className="btn" to="dashboard">
                <i className="icon-graph"></i>  Dashboard</Link>
              <Link className="btn" to="profile">
                <i className="icon-settings"></i> Profile</Link>
            </div>
          </li>
        </ol>
        <div className="container-fluid">
          <div className="animated fadeIn">            
              
            <div className="row">
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">{ all_invoices_count }</div>
                    <div>Processed Invoices Count</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-success" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Count of all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {all_invoices_vat_sum} </div>
                    <div>Total VAT</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">VAT Sum for all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {all_invoices_total_sum} </div>
                    <div>Grand Total all time</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-warning" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Total Sum for all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {invoices_failed_to_sign_count} </div>
                    <div>Failed to Sign</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Invoices that failed to process count.</small>
                  </div>
                </div>
              </div>
            </div>

            <br/>
            
            <Row>
              <Col xs={12} sm={12} md={12}>
                <Card>
                    <CardHeader>
                      <CardTitle>Invoice Signing</CardTitle>
                      <p className="card-category">Latest Invoices Signed</p>
                    </CardHeader>
                    <CardBody>
                      <AreaChart data={ arr } />
                    </CardBody>
                 </Card>   
              </Col>  
            </Row>

            <br/>

          </div>
        </div>
      </main>      
    </div>
    <footer className="app-footer">
      <div>
        <a href="http://strutstechnology.co.ke">Mobile Banking tool. </a>
        <span>&copy; Struts Technology.</span>
      </div>
      <div className="ml-auto">
        <span>Powered by</span>
        <a href="http://strutstechnology.co.ke"> Struts Technology.</a>
      </div>
    </footer>    
  </div>
    );
  }
}

export default Dashboard;
