import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Table, Form } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getUserDetails, isLoggedIn } from "../../../util/AuthService";


class Users extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Users";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      records: [],
      recordsSearch: [],
      recordsCount: 0, 
      graphArray: [],
      isLoading: false,
      pageOfItems: [], 
      currentPage: 1,
      recordsPerPage: 5, 
      startDate: new Date(), 
      endDate: new Date(), 
      reportTypes: ["Export", "Excel", "PDF", "CSV"], 
      reportExportType: "Excel" 
    };

    this.handleSearch = this.handleSearch.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleDateFilter = this.handleDateFilter.bind(this);
    this.handleReportExport = this.handleReportExport.bind(this);
    this.handleClick = this.handleClick.bind(this);
    // this.loadSidebarMenu = this.loadSidebarMenu.bind(this);

    loadSidebarMenu.bind(this); 
  }

  handleSearch(e) {
    // Variable to hold the original version of the list
    let currentList = [];
    // Variable to hold the filtered list before putting into state
    let newList = [];

    // If the search bar isn't empty
    if (e.target.value !== "") {      
        // Assign the original list to currentList
        currentList = this.state.recordsSearch;

        currentList.filter(item => {
          const lc = item.name.toLowerCase();
          const filter = e.target.value.toLowerCase();          
          if (lc.includes(filter)){
            newList.push(item); 
          }
          return newList; 
        });

        currentList.filter(item => {
          const lc = item.phone.toLowerCase();
          const filter = e.target.value.toLowerCase();          
          if (lc.includes(filter)){
            newList.push(item); 
          }
          return newList; 
        });

        currentList.filter(item => {
          const lc = item.email.toLowerCase();
          const filter = e.target.value.toLowerCase();          
          if (lc.includes(filter)){
            newList.push(item); 
          }
          return newList; 
        });       
        
    } else {
      // If the search bar is empty, set newList to original task list
      newList = this.state.recordsSearch;
    }
        // Set the filtered state based on what our rules added to newList
    this.setState({
      records: newList
    });
  }


  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  handleStartDateChange(date) {    
    this.setState({
      startDate: date
    });
  }

  handleEndDateChange(date) {
    this.setState({
      endDate: date
    });
  }

  handleReportExport(event){
    var reportType = event.target.value; 
    console.log("Downloading report >>> " + reportType);
  }

  handleDateFilter(){

    var loggedIn = getUserDetails();
    var startDate = Date.parse(this.state.startDate); 
    var endDate = Date.parse(this.state.endDate);     
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    axios({
      url: API_BASE_URL + "/users/" + startDate + "/" + endDate,
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ records: json_data.users });
          this.setState({ recordsCount: json_data.users_count });
          
          this.setState({ recordsSearch: json_data.users });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount() {
  
    this.checkAuth();
    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    axios({
      url: API_BASE_URL + "/users",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));        

        if (!response.data.error) {
          this.setState({ records: json_data.users });
          this.setState({ recordsCount: json_data.users_count });          
          this.setState({ recordsSearch: json_data.users });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  render() {
    
    const { records, currentPage, recordsPerPage, recordsCount } = this.state;

    // Logic for displaying current records 
    const indexOfLastRecord = currentPage * recordsPerPage;
    const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
    const currentRecords = records.slice(indexOfFirstRecord, indexOfLastRecord);


    // Logic for displaying page numbers
    const recordsPageNumbers = [];
    for (let i = 1; i <= Math.ceil(records.length / recordsPerPage); i++) {
      recordsPageNumbers.push(i);
    }

    const renderRecordsPageNumbers = recordsPageNumbers.map(number => {
      return (
        <li 
          className="field btn btn-primary btn-xl text-uppercase"
          color="green"
          style={{
            marginRight: "0.3em",                         
            backgroundColor: "#6bd098"
          }}
          key={number}
          id={number}
          onClick={this.handleClick}
        >              
           {number}
         
        </li>
      );
    });

    let searchBox = (    

      <div style={{ float:"right", clear:"both" }}>      

        <input style={{ marginRight: "10px", borderRadius: "5px", fontSize: "12pt", float: "right" }} 
          type="text" className="input" placeholder="Search..." onChange={this.handleSearch} />

      </div>);

    let createUserButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <div style={{ }}>       
      <Link to="create-user">
        <Form.Field
          primary
          className="field btn btn-primary btn-sm text-uppercase"
          type="submit"
          color="green"
          style={{ backgroundColor: "#6bd098" }}
          onClick={this.onSubmit}
        >
          Add User 
        </Form.Field>
      </Link>


      <div style = {{ float: "right", marginRight: "0px", zIndex: "6" }}>

      <Row>

        <Col md={4} xs={4}>
          <DatePicker
              selected={this.state.startDate}
              onChange={this.handleStartDateChange}
              dateFormat="dd/MM/yyyy" 
              // minDate= { subDays(new Date(), 5) }
              // maxDate= { new Date()}
              style={{ zIndex: "5"}}
          />
        </Col>

        <Col md={4} xs={4} style={{ marginLeft: "-10px" }}>
          <DatePicker
              selected={this.state.endDate}
              onChange={this.handleEndDateChange}
              dateFormat="dd/MM/yyyy" 
              // minDate= { subDays(new Date(), 5) }
              // maxDate= { new Date()}
              style={{ zIndex: "5"}}
          />
        </Col>

        <Col md={1} xs={1} style={{ marginLeft: "-12px" }}>
            <Form.Field
              primary
              className="field btn btn-primary btn-sm text-uppercase"
              type="submit"
              color="blue"
              style={{ backgroundColor: "#20a8d8" }}
              onClick={this.handleDateFilter}
            >
              Filter
            </Form.Field>
        </Col>

        <Col md={1} xs={1} style={{ marginLeft: "15px" }}>
        <select style={{ height: "28px"}} >
              { this.state.reportTypes.map(
                        (reportType) => 
                        <option key={reportType} value={reportType} onClick={this.handleReportExport}>{ reportType }</option>) }                                
        </select>           
        </Col>

        {/* <Col md={1} xs={1} style={{ marginLeft: "5px" }}>
          <Link to="create-user">
            <Form.Field
              primary
              className="field btn btn-primary btn-sm text-uppercase"
              type="submit"
              color="blue"
              style={{ backgroundColor: "#20a8d8" }}
              onClick={this.onSubmit}
            >
              PDF
            </Form.Field>
          </Link>
        </Col>

        <Col md={1} xs={1} style={{ marginLeft: "5px" }}>
          <Link to="create-user">
            <Form.Field
              primary
              className="field btn btn-primary btn-sm text-uppercase"
              type="submit"
              color="blue"
              style={{ backgroundColor: "#20a8d8" }}
              onClick={this.onSubmit}
            >
              CSV
            </Form.Field>
          </Link>
        </Col> */}

      </Row>
        
      </div>
      
    </div>
    );

    let viewButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#6bd098" }}
        onClick={this.onSubmit}
      >
        View
      </Form.Field>
    );

    let updateButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#4acccd", marginLeft: "5px" }}
        onClick={this.onSubmit}
      >
        Update
      </Form.Field>
    );

    return (
      <div className="">

      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu("/users") }

          {/* <Sidebar
            {...this.props}
            routes={dashboardRoutes}
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
          /> */}

          <main className="main">

        
        <div className="main-panel" ref="mainPanel">
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <Link to="dashboard">Admin</Link>
            </li>
            <li className="breadcrumb-item active">Users</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <Link className="btn" to="dashboard">
                  <i className="icon-speech"></i>
                </Link>
                <Link className="btn" to="dashboard">
                  <i className="icon-graph"></i>  Dashboard</Link>
                <Link className="btn" to="users">
                  <i className="icon-settings"></i>  Settings</Link>
              </div>
            </li>
          </ol>
          
          <div className="container-fluid">

            <Row>
              <Col xs={12}>
                <Card style={{ height: "100%"}}>
                  <CardHeader>
                    <CardTitle tag="h5">Users ({recordsCount}) &nbsp; {searchBox} </CardTitle>                    
                  </CardHeader>
                  <CardBody style={{ overflowX: "hidden", msOverflowY: "auto", height: "100%" }}>

                    {createUserButton}          

                    <br/>
                    <Table
                      celled
                      selectable
                      responsive
                      style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                    >
                      <Table.Header>
                        <Table.Row style={{ color: "#51cbce" }}>
                          <Table.HeaderCell>Name</Table.HeaderCell>
                          <Table.HeaderCell>Phone</Table.HeaderCell>
                          <Table.HeaderCell>Email</Table.HeaderCell>
                          <Table.HeaderCell>Date Created</Table.HeaderCell>                          
                          <Table.HeaderCell>Action</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        {currentRecords.map(record => (
                          <Table.Row>
                            <Table.Cell>{record.firstname} {record.lastname}</Table.Cell>
                            <Table.Cell>{record.msisdn}</Table.Cell>
                            <Table.Cell>{record.email}</Table.Cell>
                            <Table.Cell>{record.date_created}</Table.Cell>                            
                            <Table.Cell>
                              <Link to={`/user/${record.id}`}>
                                {viewButton}
                              </Link>

                              <Link to={`/user/${record.id}`}>
                                {updateButton}
                              </Link>
                            </Table.Cell>
                          </Table.Row>
                        ))}
                      </Table.Body>
                    </Table>

                    <ul style={{
                      listStyle: "none", 
                      display: "flex", 
                      marginBottom: "-20px" 
                    }}>
                      {renderRecordsPageNumbers}
                    </ul>
                    
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        />

        </main>
        </div>
        </div>
      </div>
    );
  }
}

export default Users;
