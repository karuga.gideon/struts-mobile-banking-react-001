import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Table, Form } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";
import { FaFileExcel, FaFileCsv } from 'react-icons/fa';

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getUserDetails, isLoggedIn } from "../../../util/AuthService";
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';

window.React = React;


export class CommentList extends React.Component {
    static propTypes = {
        data: PropTypes.array.isRequired,
    };

    render() {
        let commentNodes = this.props.data.map(function (invoice, index) {
            return <div key={index}>
                {invoice.id} &nbsp; | &nbsp;
          {invoice.date_created} &nbsp; | &nbsp;
          {invoice.invoice_number}
            </div>;
        });

        return (
            <div id="project-comments" className="commentList">
                <ul>{commentNodes}</ul>
            </div>
        );
    }
}

class Customers extends React.Component {

    constructor(props) {
        super(props);

        document.title = "Customers";

        this.state = {
            backgroundColor: "black",
            activeColor: "warning",
            records: [],
            currentRecords: [],
            data: [],
            offset: 0,
            recordsSearch: [],
            recordsCount: 0,
            graphArray: [],
            isLoading: false,
            pageOfItems: [],
            currentPage: 1,
            recordsPerPage: 5,
            limit: 5,
            startDate: new Date(),
            endDate: new Date(),
            reportTypes: ["Export", "Excel", "CSV"],
            reportExportType: "Excel"
        };

        this.handleSearch = this.handleSearch.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleDateFilter = this.handleDateFilter.bind(this);
        this.handleReportExport = this.handleReportExport.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }


    handleSearch(e) {
        const { limit } = this.state;
        var queryParameter = e.target.value;


        if (queryParameter.length === 0) {
            queryParameter = "query"
        }

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = loggedIn.token;
        }


        axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
        // customers/{limit}/{offset}/{search}    
        axios({
            url: API_BASE_URL + "/customers/5/0/" + queryParameter,
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({ records: json_data.customers });
                    this.setState({ data: json_data.customers });
                    this.setState({ currentRecords: json_data.customers });
                    this.setState({ recordsCount: json_data.customers_count });
                    this.setState({ offset: (json_data.offset + limit) });
                    this.setState({ recordsSearch: json_data.customers });
                    this.setState({ pageCount: json_data.num_pages });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }


    handleClick(event) {
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    handleStartDateChange(date) {
        this.setState({
            startDate: date
        });
    }

    handleEndDateChange(date) {
        this.setState({
            endDate: date
        });
    }

    handleReportExport(event) {
        var reportType = event; // event.target.value.toLowerCase(); 
        var loggedIn = getUserDetails();
        var startDate = Date.parse(this.state.startDate);
        var endDate = Date.parse(this.state.endDate);
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = loggedIn.token;
        }


        axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
        axios({
            url: API_BASE_URL + "/export/customers/" + reportType + "/" + startDate + "/" + endDate,
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                // var json_data = JSON.parse(JSON.stringify(response.data));
                // 

                if (reportType === "excel") {
                    console.log("Downloading excel report...");
                    this.downloadURL("http://localhost:9010/downloads/customers_Excel.xlsx");
                }

                if (reportType === "csv") {
                    console.log("Downloading CSV Report...");
                    // var url = "http://localhost:9010/downloads/customers_CSV.csv";
                    // this.downloadURI(url);
                    document.getElementById('download_csv').click();
                    // document.getElementById('my_iframe').src = url;
                }


            })
            .catch(err => {
                console.log(err);
            });
    }

    downloadURL(url) {
        var hiddenIFrameID = 'hiddenDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        iframe.src = url;
    };


    downloadURI(uri, name) {
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        link.click();
    }

    handleDateFilter() {

        const { limit } = this.state;
        var loggedIn = getUserDetails();
        var startDate = Date.parse(this.state.startDate);
        var endDate = Date.parse(this.state.endDate);
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = loggedIn.token;
        }

        axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
        axios({
            url: API_BASE_URL + "/customers/" + startDate + "/" + endDate,
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({ records: json_data.customers });
                    this.setState({ data: json_data.customers });
                    this.setState({ currentRecords: json_data.customers });
                    this.setState({ recordsCount: json_data.customers_count });
                    this.setState({ offset: (json_data.offset + limit) });
                    this.setState({ recordsSearch: json_data.customers });
                    this.setState({ pageCount: json_data.num_pages });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    componentDidMount() {

        const { limit, offset } = this.state;
        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = loggedIn.token;
        }

        axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
        // customers/{limit}/{offset}/{search}    
        axios({
            url: API_BASE_URL + "/customers/" + limit + "/" + offset + "/query",
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({ records: json_data.customers });
                    this.setState({ data: json_data.customers });
                    this.setState({ currentRecords: json_data.customers });
                    this.setState({ recordsCount: json_data.customers_count });
                    this.setState({ offset: (json_data.offset + limit) });
                    this.setState({ recordsSearch: json_data.customers });
                    this.setState({ pageCount: json_data.num_pages });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    handlePageClick = data => {
        this.loadcustomersFromServer(data);
    };


    loadcustomersFromServer(data) {

        var selectedPage = data.selected + 1;
        const { limit } = this.state;

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = loggedIn.token;
        }


        axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
        console.log("");
        // customers/{limit}/{offset}/{search}
        var calculatedOffset = (selectedPage * limit) - limit;
        axios({
            url: API_BASE_URL + "/customers/" + limit + "/" + calculatedOffset + "/query",
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({ records: json_data.customers });
                    this.setState({ data: json_data.customers });
                    this.setState({ currentRecords: json_data.customers });
                    this.setState({ recordsCount: json_data.customers_count });
                    this.setState({ recordsSearch: json_data.customers });
                    this.setState({ pageCount: json_data.num_pages });
                    this.setState({ offset: calculatedOffset });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.user.name,
                phone: userDetails.user.phone,
                email: userDetails.user.email
            });
        }
    }


    static propTypes = {
        data: PropTypes.array.isRequired,
    };

    render() {

        const { currentRecords, recordsCount } = this.state;

        // Logic for displaying page numbers
        // const recordsPageNumbers = [];
        // for (let i = 1; i <= Math.ceil(records.length / recordsPerPage); i++) {
        //   recordsPageNumbers.push(i);
        // }

        let searchBox = (

            <div style={{ float: "right", clear: "both" }}>

                <input style={{ marginRight: "10px", borderRadius: "5px", fontSize: "12pt", float: "right" }}
                    type="text" className="input" placeholder="Search..." onChange={this.handleSearch} />

            </div>);

        let createUserButton = this.state.isLoading ? (
            <Loader />
        ) : (

                <div style={{ paddingBottom: "30px" }}>

                    <Link to="upload-customers">
                        <Form.Field
                            // primary
                            className="field btn btn-primary btn-sm text-uppercase"
                            type="submit"
                            color="green"
                            style={{ backgroundColor: "#6bd098" }}
                            onClick={this.onSubmit}
                        >
                            Upload Customers
                        </Form.Field>
                    </Link>

                    <div style={{ float: "right", marginRight: "0px", zIndex: "6", width: "55%" }}>

                        <Row>

                            <Col md={2} xs={2} style={{ marginRight: "35px" }}>
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleStartDateChange}
                                    dateFormat="dd/MM/yyyy"
                                    // minDate= { subDays(new Date(), 5) }
                                    // maxDate= { new Date()}
                                    style={{ zIndex: "5" }}
                                />
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "45px" }}>
                                <DatePicker
                                    selected={this.state.endDate}
                                    onChange={this.handleEndDateChange}
                                    dateFormat="dd/MM/yyyy"
                                    // minDate= { subDays(new Date(), 5) }
                                    // maxDate= { new Date()}
                                    style={{ zIndex: "5" }}
                                />
                            </Col>

                            <Col md={1} xs={1} style={{ marginLeft: "75px" }}>
                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm text-uppercase"
                                    type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    onClick={this.handleDateFilter}
                                >
                                    Filter
            </Form.Field>
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "45px" }}>

                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm"
                                    // type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    // onClick={this.handleReportExport("excel")}
                                    onClick={() => this.handleReportExport("excel")}
                                >
                                    Excel <FaFileExcel />
                                    <span className="badge badge-primary"></span>
                                </Form.Field>
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "-45px" }}>
                                {/* <Link  className="nav-link" to="#" onClick={this.handleReportExport("csv")}>
            <i className="nav-icon icon-speedometer"><FaDatabase /></i>
            <span className="badge badge-primary"></span>
          </Link>   */}

                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm"
                                    type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    // onClick={this.handleReportExport("csv")}
                                    onClick={() => this.handleReportExport("csv")}
                                >
                                    CSV <FaFileCsv />
                                    <span className="badge badge-primary"></span>
                                </Form.Field>
                            </Col>


                            {/* <select style={{ height: "28px"}} >
              { this.state.reportTypes.map(
                        (reportType) => 
                        <option key={reportType} value={reportType} onClick={this.handleReportExport}>{ reportType }</option>) }                                
        </select>            */}

                        </Row>

                    </div>

                </div>
            );

        let viewButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Form.Field
                    primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    style={{ backgroundColor: "#6bd098" }}
                    onClick={this.onSubmit}
                >
                    View
                </Form.Field>
            );

        return (
            <div className="">

                <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                    {loadHeader()}

                    <div className="app-body">

                        {loadSidebarMenu("/customers-001")}

                        {/* <Sidebar
            {...this.props}
            routes={dashboardRoutes}
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
          /> */}

                        <main className="main">


                            <div className="main-panel" ref="mainPanel">

                                {/* <!-- Breadcrumb--> */}
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">Home</li>
                                    <li className="breadcrumb-item">
                                        <Link to="dashboard">Admin</Link>
                                    </li>
                                    <li className="breadcrumb-item active">customers</li>
                                    {/* <!-- Breadcrumb Menu--> */}
                                    <li className="breadcrumb-menu d-md-down-none">
                                        <div className="btn-group" role="group" aria-label="Button group">
                                            <Link className="btn" to="dashboard">
                                                <i className="icon-speech"></i>
                                            </Link>
                                            <Link className="btn" to="dashboard">
                                                <i className="icon-graph"></i>  Dashboard</Link>
                                            <Link className="btn" to="customers">
                                                <i className="icon-settings"></i>  Settings</Link>
                                        </div>
                                    </li>
                                </ol>

                                <div className="container-fluid">

                                    <Row>
                                        <Col xs={12}>
                                            <Card style={{ height: "100%" }}>
                                                <CardHeader>
                                                    <CardTitle tag="h5">Customers ({recordsCount}) &nbsp; {searchBox} </CardTitle>
                                                </CardHeader>
                                                <CardBody style={{ overflowX: "hidden", msOverflowY: "auto", height: "100%" }}>

                                                    {createUserButton}

                                                    <a href="http://localhost:9010/downloads/customers_CSV.csv" download id="download_csv" hidden>Download</a>


                                                    <br />
                                                    <Table
                                                        celled
                                                        selectable
                                                        responsive
                                                        style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                                                    >
                                                        <Table.Header>
                                                            <Table.Row style={{ color: "#51cbce" }}>
                                                                <Table.HeaderCell>Customer Name</Table.HeaderCell>
                                                                <Table.HeaderCell>Phone Number</Table.HeaderCell>
                                                                <Table.HeaderCell>Account Number</Table.HeaderCell>
                                                                <Table.HeaderCell>Email Address</Table.HeaderCell>
                                                                <Table.HeaderCell>Date Created</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>

                                                        <Table.Body>
                                                            {currentRecords.map(record => (
                                                                <Table.Row key={record.id}>
                                                                    <Table.Cell>{record.customer_name}</Table.Cell>
                                                                    <Table.Cell>{record.phone_number}</Table.Cell>
                                                                    <Table.Cell>{record.account_number}</Table.Cell>
                                                                    <Table.Cell>{record.email_address}</Table.Cell>
                                                                    <Table.Cell>{record.date_created}</Table.Cell>
                                                                    <Table.Cell>
                                                                        <Link to={`/user/${record.id}`}>
                                                                            {viewButton}
                                                                        </Link>
                                                                    </Table.Cell>
                                                                </Table.Row>
                                                            ))}
                                                        </Table.Body>
                                                    </Table>

                                                    <br />

                                                    <div className="container">
                                                        {/* <CommentList data={this.state.data} /> */}
                                                        <nav aria-label="Page navigation example">
                                                            <ReactPaginate
                                                                previousLabel={'Previous'}
                                                                nextLabel={'Next'}
                                                                breakLabel={'...'}
                                                                breakClassName={'break-me'}
                                                                pageCount={this.state.pageCount}
                                                                marginPagesDisplayed={2}
                                                                pageRangeDisplayed={5}
                                                                onPageChange={this.handlePageClick}
                                                                containerClassName={'pagination'}
                                                                subContainerClassName={'pages pagination page-item'}
                                                                activeClassName={'active'}
                                                                pageClassName="page-item"
                                                                pageLinkClassName="page-link"
                                                                disabledClassName="disabled"
                                                                nextClassName="page-link"
                                                                previousClassName="page-link"
                                                            />
                                                        </nav>
                                                    </div>

                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />

                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default Customers;
