import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

// import { Line, Pie } from "react-chartjs-2";
// import Stats from "../../../components/admin/Stats/Stats.jsx";

// import {
//   dashboardEmailStatisticsChart,
//   dashboardNASDAQChart
// } from "../../../variables/charts.jsx";

import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";
import PerfectScrollbar from "perfect-scrollbar";
import { Link } from "react-router-dom";

import { BarChart, ColumnChart   } from 'react-chartkick'; 

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import { getUserDetails, isLoggedIn } from "../../../util/AuthService";


var ps;
// const element = <Sidebar activePage="dashboard" />;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    // this.props.activePage = "dashboard";
    document.title = "Dashboard";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning", 
      invoicesChartData: [] 
    };
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel);
      document.body.classList.toggle("perfect-scrollbar-on");
    }

    
    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
        
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    axios({
      url: API_BASE_URL + "/reports/summary",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));

        if (!response.data.error) {
          this.setState({ records: json_data });
          this.setState({ graphArray: json_data });
          this.setState({
            all_invoices_count: json_data.all_invoices_count.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            all_invoices_vat_sum: json_data.all_invoices_vat_sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            all_invoices_total_sum: json_data.all_invoices_total_sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            invoices_failed_to_sign_count: json_data.invoices_failed_to_sign_count.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
          });
        }
      })
      .catch(err => {
        console.log(err);
      });


      // Retrieve Invoices Chart Data 
      axios({
        url: API_BASE_URL + "/dashboard/invoices/chart",
        method: "GET",
        withCredentials: false
      })
        .then(response => {
          var json_data = JSON.parse(JSON.stringify(response.data));
  
          if (!response.data.error) {
            this.setState({ invoicesChartData: json_data });            
          }
        })
        .catch(err => {
          console.log(err);
        });        
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      this.refs.mainPanel.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  
  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };



  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  render() {


    const { all_invoices_count, all_invoices_vat_sum, all_invoices_total_sum, 
      invoices_failed_to_sign_count, invoicesChartData } = this.state;

    var arr = {};

    invoicesChartData.forEach(processData); 
    function processData(record, index){
      var babyArr = {};
      babyArr[0] = record.date;
      babyArr[1] = record.records_count;
      arr[record.date] = record.records_count;
    }

    return (

      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          {/* <Sidebar
            {...this.props}
            routes={dashboardRoutes}
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
          />
           */}

          <main className="main">
            <div
              className=""
              style={{
                overflowX: "hidden",
                overflowY: "hidden",
                msOverflowY: "hidden"
              }}
            >
        
        <div
          className="main-panel"
          ref="mainPanel"
          style={{
            overflowX: "hidden",
            overflowY: "hidden",
            msOverflowY: "hidden"
          }}
        >
          
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <Link to="dashboard">Admin</Link>
            </li>
            <li className="breadcrumb-item active">Reports</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <Link className="btn" to="dashboard">
                  <i className="icon-speech"></i>
                </Link>
                <Link className="btn" to="dashboard">
                  <i className="icon-graph"></i>  Dashboard</Link>
                <Link className="btn" to="reports">
                  <i className="icon-settings"></i>  Settings</Link>
              </div>
            </li>
          </ol>
          <div className="container-fluid">
            {/* <Row>
              <Col xs={12} sm={12} md={4}>
                <Card>
                  <CardHeader>
                    <CardTitle>Collection Statistics</CardTitle>
                    <p className="card-category">Latest Collections Performance</p>
                  </CardHeader>
                  <CardBody>
                    <Pie
                      data={dashboardEmailStatisticsChart.data}
                      options={dashboardEmailStatisticsChart.options}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="legend">
                      <i className="fa fa-circle text-primary" /> Paid{" "} &nbsp; &nbsp; 
                      <i className="fa fa-circle text-warning" /> Credits{" "} &nbsp; &nbsp; 
                      <i className="fa fa-circle text-danger" /> Defaulted{" "} &nbsp; &nbsp; 
                      <i className="fa fa-circle text-gray" /> Pending
                    </div>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-calendar-alt",
                          t: " Number of collections"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
              <Col xs={12} sm={12} md={8}>
                <Card className="card-chart">
                  <CardHeader>
                    <CardTitle>CREDITS: PAYMENTS</CardTitle>
                    <p className="card-category">Line Chart With Points</p>
                  </CardHeader>
                  <CardBody>
                    <Line
                      data={dashboardNASDAQChart.data}
                      options={dashboardNASDAQChart.options}
                      width={400}
                      height={100}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="chart-legend">
                      <i className="fa fa-circle text-info" /> Payments {" "} &nbsp; &nbsp; 
                      <i className="fa fa-circle text-warning" />  Credits
                    </div>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-check",
                          t: " Data information certified"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
            </Row>

            <br/><br/> */}

            <div className="row">
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0">{ all_invoices_count }</div>
                    <div>Processed Invoices Count</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-success" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Count of all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {all_invoices_vat_sum} </div>
                    <div>Total VAT</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">VAT Sum for all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {all_invoices_total_sum} </div>
                    <div>Grand Total all time</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-warning" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Total Sum for all processed invoices.</small>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <div className="h4 m-0"> {invoices_failed_to_sign_count} </div>
                    <div>Failed to Sign</div>
                    <div className="progress progress-xs my-1">
                      <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%",  ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                    </div>
                    <small className="text-muted">Invoices that failed to process count.</small>
                  </div>
                </div>
              </div>
            </div>

            <br/>

            <Row>
              <Col xs={6} sm={6} md={6}>
                <Card>
                    <CardHeader>
                      <CardTitle>Invoice Signing</CardTitle>
                      <p className="card-category">Latest Invoices Signed</p>
                    </CardHeader>
                    <CardBody>
                      <BarChart  data={ arr } />
                    </CardBody>
                 </Card>   
              </Col>  

              <Col xs={6} sm={6} md={6}>
                <Card>
                    <CardHeader>
                      <CardTitle>Invoice Signing</CardTitle>
                      <p className="card-category">Latest Invoices Signed</p>
                    </CardHeader>
                    <CardBody>
                      <ColumnChart  data={ arr } />
                    </CardBody>
                 </Card>   
              </Col>  
            </Row>

          </div>
          {/* <Footer fluid /> */}
        </div>
        {/* <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        /> */}
      </div>

      </main>
      </div>
      </div>
    );
  }
}

export default Dashboard;
