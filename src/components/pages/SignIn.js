import React from "react";
import Footer from "../shared/Footer";
import { Link } from "react-router-dom";
import { Form } from "semantic-ui-react";
import axios from "axios";
import { API_BASE_URL, PROFILE_PAYLOAD } from "../../constants";
import Loader from "../shared/Loader";

class App extends React.Component {

  constructor(props) {
    super(props);

    document.title = "Mobile Banking : Sign In";

    this.state = {
      email: "",
      password: "",
      isLoading: false
    };
  }

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  onSubmit = e => {

    this.setState({ isLoading: true });
    console.log("User login...");
    var basicAuth = 'Basic ' + btoa(this.state.email + ':' + this.state.password);

    axios({
      url: `${API_BASE_URL}/authenticate`,
      method: "GET",
      withCredentials: false,
      headers: {
        "Authorization": basicAuth
      }
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          localStorage.setItem(PROFILE_PAYLOAD, JSON.stringify(response.data));
          this.props.history.push("/dashboard");
          localStorage.setItem("access_token", response.data.token);
        } else {
          alert("Invalid Login!");
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    
    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Sign In
      </Form.Field>
    );

    return (
      <div>
        {/* Navigation */}
        {/* <Navbar /> */}

        {/* Contact */}
        <section id="contact">
          <div className="container">
            <div className="row" style={{ marginTop: "-120px" }}>
              <div className="col-lg-12 text-center">
                <div id="mainNav">
                  <Link className="navbar-brand " to="/">
                    Struts Credit Control
                  </Link>
                </div>
                <h4 className="section-heading text-uppercase">Sign In</h4>
                {/* <h3 className="section-subheading text-muted">
                      Kindly fill in all the details.
                    </h3> */}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3" />
              <div className="col-lg-6">
                <Form>
                  <Form.Field className="form-control">
                    <input
                      type="email"
                      placeholder="email or username"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    <input
                      type="password"
                      placeholder="password"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                      onKeyPress={event => {
                        if (event.key === 'Enter') {
                          this.onSubmit()
                        }
                      }}
                    />
                  </Form.Field>
                  <br />

                  <div className="col-lg-12 text-center">
                    <br />
                    {button}
                  </div>

                </Form>

                <br />
                
              </div>
            </div>
          </div>
        </section>

        {/* Footer */}
        <Footer />

      </div>
    );
  }
}

export default App;
