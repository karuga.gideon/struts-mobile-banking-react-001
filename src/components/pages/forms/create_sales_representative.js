import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

import { Form, Label } from "semantic-ui-react";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { getUserDetails } from "../../../util/AuthService";

class CreateSalesRepresentative extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Create Sales Representative";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      name: "",
      phone: "",
      email: ""
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  onSubmit = e => {
    this.setState({ isLoading: true });
    
    var clientPayLoad = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email 
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Creating Survey...");
    console.log(clientPayLoad);

    axios({
      url: `${API_BASE_URL}/salesrepresentatives`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: clientPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.error) {
          this.props.history.push("/sales_reps");          
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Add Sales Representative 
      </Form.Field>
    );

    let backButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Link
        to="/sales_reps"
        className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
        style={{ backgroundColor: "rgb(107, 208, 152)" }}
      >
        <Menu.Item name="Back" />
      </Link>
    );

    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Add Sales Representative</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>

                      {/* Name  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Name
                        </Label>
                        <Form.Field className="form-control">                          
                          <input
                            type="text"
                            placeholder="Name*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ name: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      {/* Phone Number  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Phone Number
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Phone Number*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ phone: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>

                      {/* Email  */}'
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Email Address
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Email Address*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ email: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      <div className="col-lg-12">                        
                        {button} &nbsp; {backButton}
                        <br />
                        <br />
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default CreateSalesRepresentative;
