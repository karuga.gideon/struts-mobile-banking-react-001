import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

import { Form, Label, TextArea } from "semantic-ui-react";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";

class CreateSurvey extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Create Survey";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      title: "",
      description: "",
      target_number: "",
      org_id: ""
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  onSubmit = e => {
    this.setState({ isLoading: true });

    // Format the target number - int required
    var targetNo = this.state.target_number.replace(/,/g, "");

    var surveyPayLoad = {
      title: this.state.title,
      description: this.state.description,
      target_number: parseInt(targetNo),
      org_id: 5
    };
    console.log("Creating Survey...");
    console.log(surveyPayLoad);

    axios({
      url: `${API_BASE_URL}/create_survey`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: surveyPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          this.props.history.push("/surveys");
          // localStorage.setItem("OTP", response.data.typeId);
          // this.props.setUser({
          //   msisdn: response.data.msisdn,
          //   name: response.data.name
          // })
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Create Survey
      </Form.Field>
    );

    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Create Survey</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Survey Name
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="text"
                            placeholder="Survey Name*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ title: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Target Number
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="text"
                            placeholder="Target Number"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ target_number: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>

                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Short Description
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <TextArea
                            type="text"
                            placeholder="Short Description"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ description: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />

                        {button}
                        <br />
                        <br />
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default CreateSurvey;
