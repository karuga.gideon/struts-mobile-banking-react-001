import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";


import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import {
  isLoggedIn,
  getUserDetails 
} from "../../../util/AuthService";

import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { Form, Label } from "semantic-ui-react";
import axios from "axios";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Change Password";

    this.state = {
      records: [],
      isLoading: false,
      pageOfItems: [],      
      backgroundColor: "black",
      activeColor: "warning"           
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {
    this.checkAuth();
  }

  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email, 
        username: userDetails.user.username, 
        user_id: userDetails.user.id });  
    }
  }

  onSubmit = e => {

    this.setState({ isLoading: true });   

    if (this.state.password !== this.state.confirm_password){
      alert("Passwords do not match!");
      this.props.history.push("/change-password");
    }

    var userPayLoad = {    
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      username: this.state.username,   
      password: this.state.password
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    var user_id = loggedIn.user.id; 
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + user_id);
    console.log(userPayLoad);

    axios({
      url: `${API_BASE_URL}/users/` + user_id,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("Password Updated Successfully.");
          this.props.history.push("/profile");
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {   

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Change Password
      </Form.Field>
    );
    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 

        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
              <li className="breadcrumb-item">Home</li>
              <li className="breadcrumb-item">
                <a href="dashboard">Admin</a>
              </li>
              <li className="breadcrumb-item active">Profile</li>
              <li className="breadcrumb-item active">Change Password</li>
              {/* <!-- Breadcrumb Menu--> */}
              <li className="breadcrumb-menu d-md-down-none">
                <div className="btn-group" role="group" aria-label="Button group">
                  <a className="btn" href="dashboard">
                    <i className="icon-speech"></i>
                  </a>
                  <a className="btn" href="./">
                    <i className="icon-graph"></i>  Dashboard</a>
                  <a className="btn" href="users">
                    <i className="icon-settings"></i>  Settings</a>
                </div>
              </li>
            </ol>
            <div className="container-fluid">

            <Row>              
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Change Password</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Password
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="password"
                          placeholder="Password*"
                          className="form-control"
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ password: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />                      
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Confirm Password
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="password"
                          placeholder="Confirm Password*"
                          className="form-control"
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ confirm_password: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Row>
                        <div className="">
                          {button}
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
              <Col md={4} xs={12}>
                <Card className="card-user">       
                  <CardHeader>
                    <CardTitle>Change Password</CardTitle>
                  </CardHeader>           
                  <CardBody>
                  <div className="list-group">
                      <Link to="/about" className="list-group-item list-group-item-action">
                        About
                      </Link>
                      <Link to="/invoices" className="list-group-item list-group-item-action">Invoices </Link>
                      <Link to="/reports" className="list-group-item list-group-item-action">Reports</Link>
                      <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                      <Link to="/profile" className="list-group-item list-group-item-action active">Profile</Link>
                      <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                    </div>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default ChangePassword;
