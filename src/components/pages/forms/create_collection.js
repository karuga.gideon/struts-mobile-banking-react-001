import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";
import { Form, Label } from "semantic-ui-react";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getUserDetails, isLoggedIn } from "../../../util/AuthService";

class CreateCollection extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Create Client";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      client_id: "",
      invoice_number: "",
      credit_period: "",
      amount: "", 
      advance_payment: "", 
      balance: "", 
      sales_representative_id: "", 
      sales_commission: "", 
      dueDate: new Date(), 
      invoiceDate: new Date(), 
      startDate: new Date(), 
      clients: [],
      salesRepresentatives: []
    };
    this.handleDueDateChange = this.handleDueDateChange.bind(this);
    this.handleInvoiceDateChange = this.handleInvoiceDateChange.bind(this);
  }

  handleDueDateChange(date) {
    this.setState({
      dueDate: date
    });
  }

  handleInvoiceDateChange(date) {
    this.setState({
      invoiceDate: date
    });
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  onSubmit = e => {
    this.setState({ isLoading: true });
    
    var collectionPayLoad = {
      client_id: Number(this.state.client_id),
      invoice_number: this.state.invoice_number,
      credit_period: this.state.credit_period,
      amount: Number(this.state.amount),
      advance_payment: Number(this.state.advance_payment),
      balance: Number(this.state.balance),
      sales_representative_id: Number(this.state.sales_representative_id),
      sales_commission: Number(this.state.sales_commission),
      invoice_date: this.state.invoiceDate, 
      due_date: this.state.dueDate 
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Creating Collection ...");
    console.log(collectionPayLoad);

    axios({
      url: `${API_BASE_URL}/collections`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: collectionPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          this.props.history.push("/collections");          
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });      

  };

  componentDidMount(){

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Retrieving clients....?");

    axios({
        url: API_BASE_URL + "/clients",
        method: "GET",
        withCredentials: false
      })
        .then(response => {
          var json_data = JSON.parse(JSON.stringify(response.data));
          console.log("Printing clients...");
          
          this.setState({ clients: json_data.clients });
        })
        .catch(err => {
          console.log(err);
        });

    // Retrieve salesRepresentatives 
    console.log("Retrieving salesrepresentatives....?");

    axios({
        url: API_BASE_URL + "/salesrepresentatives",
        method: "GET",
        withCredentials: false
        })
        .then(response => {
            var json_data = JSON.parse(JSON.stringify(response.data));
            console.log("Printing salesrepresentatives...");
            
            this.setState({ salesRepresentatives: json_data.sales_reps });
        })
        .catch(err => {
            console.log(err);
        });
            
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }


  render() {
  
    const { clients, salesRepresentatives } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Add Collection 
      </Form.Field>
    );

    
    let backButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Link
        to="/collections"
        className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
        style={{ backgroundColor: "rgb(107, 208, 152)" }}
      >
        <Menu.Item name="Back" />
      </Link>
    );

    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <a href="dashboard">Admin</a>
            </li>
            <li className="breadcrumb-item active">Collections</li>
            <li className="breadcrumb-item active">Add Collection</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <a className="btn" href="dashboard">
                  <i className="icon-speech"></i>
                </a>
                <a className="btn" href="dashboard">
                  <i className="icon-graph"></i>  Dashboard</a>
                <a className="btn" href="users">
                  <i className="icon-settings"></i>  Settings</a>
              </div>
            </li>
          </ol>
          <div className="container-fluid">

            <Row>
              <Col md={6} xs={6}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Create Collection</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>

                      {/* Client Name  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Client Name
                        </Label>
                        <Form.Field className="form-control">
                            <select onChange={e =>
                              this.setState({ client_id: e.target.value })
                            }>
                                { clients.map(
                                        (client) => 
                                        <option key={client.id} 
                                            value={client.id} >{client.name}</option>) }                                
                            </select>                       
                        </Form.Field>
                      </div>
                      <br />

                      {/* Phone Number  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Invoice Number
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Invoice Number*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ invoice_number: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br/>                                           

                      {/* Amount  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Amount
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="number"
                            placeholder="Amount"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ amount: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>

                      {/* Advanced Payment  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Advanced Payment
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="number"
                            placeholder="Advanced Payment"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ advance_payment: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>

                      {/* Balance  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Balance
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="number"
                            placeholder="Balance"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ balance: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>
                      
                      <div className="col-lg-12">                        
                        {button} &nbsp; {backButton}
                        <br />
                        <br />
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </Col>

                <Col md={6} xs={6}>
                  <Card className="card-user">
                    <CardHeader>

                      {/* Invoice Date  */} 
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Invoice Date
                        </Label>
                        <Form.Field className="form-control">
                          <DatePicker
                              selected={this.state.startDate}
                              onChange={this.handleInvoiceDateChange}
                          />
                        </Form.Field>
                      </div>
                      <br/>

                      {/* Due Date  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Due Date
                        </Label>
                        <Form.Field className="form-control">
                          <DatePicker
                              selected={this.state.startDate}
                              onChange={this.handleDueDateChange}
                          />
                        </Form.Field>
                      </div>                                      

                       {/* Credit Period  */}'
                       <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Credit Period
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Credit Period*"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ credit_period: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      {/* Sales Representative  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Sales Representative
                        </Label>
                        <Form.Field className="form-control">
                        <select onChange={e =>
                              this.setState({ sales_representative_id: e.target.value })
                            }>
                                { salesRepresentatives.map(
                                        (salesRepresentative) => <option key={salesRepresentative.id} value={salesRepresentative.id}>{salesRepresentative.name}</option>) }
                            </select>                       
                        </Form.Field>
                        <br />
                      </div>


                      {/* Sales Commission  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Sales Commission
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="number"
                            placeholder="Sales Commission"
                            className="form-control"
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ sales_commission: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>


                    </CardHeader>
                  </Card>
                </Col>  
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default CreateCollection;
