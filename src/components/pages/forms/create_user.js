import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

import { Form, Label } from "semantic-ui-react";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { getUserDetails } from "../../../util/AuthService";

class CreateUser extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Create Client";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      name: "",
      firstname: "",
      lastname: "",
      phone: "",
      email: "",
      username: "", 
      password :"", 
      userTypes: [], 
      user_type_id: 0 
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount(){

    // this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

     // Retrieve User Types... 
     console.log("Retrieving user types....?");

     axios({
         url: API_BASE_URL + "/usertypes",
         method: "GET",
         withCredentials: false
         })
         .then(response => {
             var json_data = JSON.parse(JSON.stringify(response.data));
             console.log("Printing usertypes...");
             
             this.setState({ userTypes: json_data.user_types });
         })
         .catch(err => {
             console.log(err);
         });

  }
  onSubmit = e => {
      
    this.setState({ isLoading: true });
    
    var clientPayLoad = {      
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      name: this.state.firstname + " " + this.state.lastname, 
      phone: this.state.phone,
      msisdn: this.state.phone,
      email: this.state.email,
      username: this.state.username,
      password: this.state.password
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Creating User...");
    console.log(clientPayLoad);

    axios({
      url: `${API_BASE_URL}/users`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: clientPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          this.props.history.push("/users");          
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        onClick={this.onSubmit}
      >
        Add User 
      </Form.Field>
    );

    let backButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Link
        to="/users"
        className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
        style={{ backgroundColor: "rgb(107, 208, 152)" }}
      >
        <Menu.Item name="Back" />
      </Link>
    );

    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
          { loadHeader() }

           <div className="app-body">
        
            { loadSidebarMenu() }

        <main className="main">
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          

          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
              <li className="breadcrumb-item">Home</li>
              <li className="breadcrumb-item">
                <a href="dashboard">Admin</a>
              </li>
              <li className="breadcrumb-item active">Users</li>
              <li className="breadcrumb-item active">Add User</li>
              {/* <!-- Breadcrumb Menu--> */}
              <li className="breadcrumb-menu d-md-down-none">
                <div className="btn-group" role="group" aria-label="Button group">
                  <a className="btn" href="dashboard">
                    <i className="icon-speech"></i>
                  </a>
                  <a className="btn" href="dashboard">
                    <i className="icon-graph"></i>  Dashboard</a>
                  <a className="btn" href="clients">
                    <i className="icon-settings"></i>  Settings</a>
                </div>
              </li>
            </ol>
            <div className="container-fluid">

            <Row>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Add User</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>

                      {/* First Name  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          First Name
                        </Label>
                        <Form.Field className="form-control">                          
                          <input
                            type="text"
                            placeholder="First Name*"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ firstname: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      {/* Last Name  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Last Name
                        </Label>
                        <Form.Field className="form-control">                          
                          <input
                            type="text"
                            placeholder="Last Name*"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ lastname: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      {/* Phone Number  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Phone Number
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Phone Number*"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ phone: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>

                      {/* Email  */}'
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Email Address
                        </Label>
                        <Form.Field className="form-control">
                          <input
                            type="text"
                            placeholder="Email Address*"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ email: e.target.value })
                            }
                          />
                        </Form.Field>
                      </div>
                      <br />

                      {/* Username  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Username
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="text"
                            placeholder="Username"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ username: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>

                      {/* Password  */}
                      <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          Password
                        </Label>
                        <Form.Field className="form-control">
                          {/* <label>Oranization Name</label> */}
                          <input
                            type="password"
                            placeholder="Password"
                            className=""
                            style={{
                              textDecoration: "none",
                              padding: "0px",
                              border: "0px"
                            }}
                            onChange={e =>
                              this.setState({ password: e.target.value })
                            }
                          />
                        </Form.Field>
                        <br />
                      </div>

                      {/* User Type  */}
                      {/* <div className="col-lg-12">
                        <Label
                          style={{
                            fontSize: "0.8571em",
                            marginBottom: "5px",
                            color: "#9A9A9A"
                          }}
                        >
                          User Type
                        </Label>
                        <Form.Field className="form-control">
                        <select onChange={e =>
                              this.setState({ user_type_id: e.target.value })
                            }>
                                { userTypes.map(
                                        (userType) => <option key={userType.id} value={userType.id}>{userType.name}</option>) }
                        </select>                       
                        </Form.Field>
                        <br />
                      </div> */}

                      <div className="col-lg-12">                        
                        {button} &nbsp; {backButton}
                        <br />
                        <br />
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default CreateUser;
