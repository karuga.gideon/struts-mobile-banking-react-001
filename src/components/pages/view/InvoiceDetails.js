import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";


import { Link } from "react-router-dom";
import { Form } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import axios from "axios";

import pdfFile from '../../../Invoice.pdf'


class InvoiceDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Invoice Details";

    var invoiceID = this.props.match.params.invoiceID;
    console.log("invoiceID >>> " + invoiceID);

    this.state = {
      records: [],
      invoice_details: {},
      isLoading: false,
      pageOfItems: [],
      invoiceID: invoiceID,      
      backgroundColor: "black",
      activeColor: "warning" 
    };

    this.openPdfFile = this.openPdfFile.bind(this);
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();       
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    const { invoiceID } = this.state;
    console.log("Getting Invoice Details >>> " + invoiceID);

    axios({
      url: `${API_BASE_URL}/invoices/id/` + invoiceID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ 
              invoice_details: json_data, 
              userUpdateName: json_data.name, 
              userUpdatePhone: json_data.phone, 
              userUpdateEmail: json_data.email, 
              userUpdateUsername: json_data.username  });
          
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { invoice_details, invoiceID } = this.state;

    var userPayLoad = {
        name: this.state.userUpdateName,
        phone: this.state.userUpdatePhone,
        email: this.state.userUpdateEmail,
        username: this.state.userUpdateUsername
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + invoiceID);
    console.log(invoice_details);

    axios({
      url: `${API_BASE_URL}/users/` + invoiceID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("User Updated Successfully.");
          this.props.history.push("/user-details/" + invoiceID);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

 openPdfFile(pageURL){    
    console.log("Opening PDF URL >>>> " + pageURL);
    // var strWindowFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";
    // window.open(pageURL, "Invoice_Window", strWindowFeatures);
    // window.location.href = pageURL;
    window.open(pageURL, '_blank');
 }


  render() {

    const { invoice_details, invoiceID } = this.state;
    
    // var pdfFile = "file:///"+invoice_details.file_name;
    // var pdfFile = invoice_details.file_name;
    // var pdfFile = {process.env.PUBLIC_URL + '/pdf/invoice.pdf'};
    var apiPdfFile = "http://localhost:9010/esdws/invoices/"+invoiceID+"/pdf"; 

    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 
      
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <Link to="dashboard">Admin</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="invoices">Invoices</Link>
            </li>
            <li className="breadcrumb-item active">Invoice Details</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <Link className="btn" to="dashboard">
                  <i className="icon-speech"></i>
                </Link>
                <Link className="btn" to="/dashboard">
                  <i className="icon-graph"></i>  Dashboard</Link>
                <Link className="btn" to="/users">
                  <i className="icon-settings"></i>  Settings</Link>
              </div>
            </li>
          </ol>
          <div className="container-fluid">

            <Row>
              <Col md={8} xs={8}>
                <Card className="card-user">  
                  <CardHeader>
                    <CardTitle>Invoice Details</CardTitle>
                  </CardHeader>                
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                                            
                      Invoice Number : {invoice_details.invoice_number} <br />
                      VAT : {invoice_details.vat} <br />
                      Grand Total : {invoice_details.grand_total} <br />
                      Signature : {invoice_details.signature} <br />
                      PDF File : {invoice_details.file_name} <br />
                      Date Created : {invoice_details.date_created} <br/>
                      Invoice ID : {invoiceID} <br />

                      <br/>
                      <br/>

                      <div style={{ textAlign: ""}}>     
                        <Form.Field
                            primary
                            className="field btn btn-primary btn-sm"
                            // type="submit"
                            color="blue"
                            style={{ backgroundColor: "#20a8d8" }}
                            // onClick={this.handleReportExport("excel")}
                            onClick={() => this.openPdfFile(pdfFile)}
                            > 
                            Print PDF  
                          <span className="badge badge-primary"></span>
                        </Form.Field>                                      

                          <br/>

                          {/* <Link
                            to="/invoices"
                            className="btn btn-primary btn-xl js-scroll-trigger"
                            style={{ backgroundColor: "#fed136", width: "50%" }}
                          >
                            <Menu.Item name="Back" />
                          </Link> */}
                      </div>  

                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={4} xs={4}>
              <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Invoices</CardTitle>
                  </CardHeader>
                  <CardBody>
                                          
                      <div className="list-group">
                        <Link to="/about" className="list-group-item list-group-item-action"> About </Link>
                        <Link to="/invoices" className="list-group-item list-group-item-action active">Invoices / Back </Link>
                        <Link to="/reports" className="list-group-item list-group-item-action">Reports</Link>
                        <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                        <Link to="/profile" className="list-group-item list-group-item-action">Profile</Link>
                        <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                      </div>

                  </CardBody>
                </Card>
              </Col>
            </Row>
            
            <div style={{ width: "100%", height: "100%", paddingRight: "5px"  }}>
            <Row style={{ width: "100%", height: "900px", marginLeft: "4px"}}>
              <Card style={{ width: "100%", height: "100%" }}>
                <CardBody>
                {/* Loading PDF File : {pdfFile} <br/>                 */}
                {/* <iframe src={apiPdfFile} title="title">
                    Presss me: <a href={apiPdfFile}>Download PDF</a>
                </iframe> */}
                {/* <iframe src={apiPdfFile} width="100%" height="100%"></iframe> */}

                <embed src={apiPdfFile} width="100%" height="100%"></embed>

                {/* <object src={apiPdfFile} width="100%" height="100%"></object> */}
                </CardBody>
              </Card>
            </Row>
          </div>

          </div>         
         
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default InvoiceDetails;
