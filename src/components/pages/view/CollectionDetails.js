import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import Loader from "../../shared/Loader";
import { Form, Label } from "semantic-ui-react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";

import axios from "axios";
import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";

class CollectionDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Collection Details";

    var collectionID = this.props.match.params.collectionID;
    console.log("collectionID >>> " + collectionID);

    this.state = {
      records: [],
      collectionDetails: {},
      isLoading: false,
      pageOfItems: [],
      collectionID: collectionID,      
      backgroundColor: "black",
      activeColor: "warning", 
      dueDate: new Date(), 
      invoiceDate: new Date(), 
      startDate: new Date(), 
      clients: [],
      salesRepresentatives: []
    };

    this.handleDueDateChange = this.handleDueDateChange.bind(this);
    this.handleInvoiceDateChange = this.handleInvoiceDateChange.bind(this);
  }

  handleDueDateChange(date) {
    this.setState({
      dueDate: date
    });
  }

  handleInvoiceDateChange(date) {
    this.setState({
      invoiceDate: date
    });
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    const { collectionID } = this.state;
    console.log("Getting Client Details >>> " + collectionID);

    axios({
      url: `${API_BASE_URL}/collections/` + collectionID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ 
              collectionDetails: json_data, 
              collectionUpdateDateCreated: json_data.collection.date_created, 
              collectionUpdateClientID: json_data.collection.client_id, 
              collectionUpdateInvoiceNumber: json_data.collection.invoice_number, 
              collectionUpdateInvoiceDate: json_data.collection.invoice_date, 
              collectionUpdateDueDate: json_data.collection.due_date, 
              collectionUpdateCreditPeriod: json_data.collection.credit_period,
              collectionUpdateAmount: json_data.collection.amount,
              collectionUpdateAdvancePayment: json_data.collection.advance_payment, 
              collectionUpdateBalance: json_data.collection.balance, 
              collectionUpdateSalesRepresentativeID: json_data.collection.sales_representative_id,
              collectionUpdateSalesCommission: json_data.collection.sales_commission, 
              collectionUpdateClientName: json_data.client.name, 
              collectionUpdateSalesRepName: json_data.sales_representative.name, 
              collectionUpdateSalesRepID: json_data.sales_representative.id, 
              dueDate: new Date(json_data.collection.due_date), 
              invoiceDate: new Date(json_data.collection.invoice_date) 
             });
          
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });

    console.log("Retrieving clients....?");
    axios({
        url: API_BASE_URL + "/clients",
        method: "GET",
        withCredentials: false
      })
        .then(response => {
          var json_data = JSON.parse(JSON.stringify(response.data));
          console.log("Printing clients...");
          
          this.setState({ clients: json_data.clients });
        })
        .catch(err => {
          console.log(err);
        });

    // Retrieve salesRepresentatives 
    console.log("Retrieving salesrepresentatives....?");
    axios({
        url: API_BASE_URL + "/salesrepresentatives",
        method: "GET",
        withCredentials: false
        })
        .then(response => {
            var json_data = JSON.parse(JSON.stringify(response.data));
            console.log("Printing salesrepresentatives...");
            
            this.setState({ salesRepresentatives: json_data.sales_reps });
        })
        .catch(err => {
            console.log(err);
        });
            
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { collectionDetails, collectionID } = this.state;
  
    var collectionPayLoad = {
        invoice_number: this.state.collectionUpdateInvoiceNumber,
        credit_period: this.state.collectionUpdateCreditPeriod,
        amount: Number(this.state.collectionUpdateAmount), 
        advance_payment: Number(this.state.collectionUpdateAdvancePayment), 
        balance: Number(this.state.collectionUpdateBalance),         
        sales_commission: Number(this.state.collectionUpdateSalesCommission), 
        invoice_date: this.state.invoiceDate, 
        due_date: this.state.dueDate, 
        client_id: Number(this.state.collectionUpdateClientID), 
        sales_representative_id: Number(this.state.collectionUpdateSalesRepID)
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + collectionID);
    console.log(collectionDetails);

    axios({
      url: `${API_BASE_URL}/collections/` + collectionID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: collectionPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("Collection Updated Successfully.");
          this.props.history.push("/collection-details/" + collectionID);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    const { collectionUpdateDueDate, collectionUpdateAmount, collectionUpdateCreditPeriod, collectionUpdateBalance, 
            collectionUpdateInvoiceNumber, collectionUpdateSalesRepName, collectionUpdateAdvancePayment, 
            collectionUpdateInvoiceDate, collectionUpdateClientName, collectionUpdateSalesCommission, 
            collectionUpdateClientID, collectionUpdateSalesRepID } = this.state;

    const { clients, salesRepresentatives } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update Collection 
      </Form.Field>
    );
    
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">                  
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> Collection Details </h6>
                      Client Name : {collectionUpdateClientName} <br />
                      Invoice Number : {collectionUpdateInvoiceNumber} <br />
                      Amount: {collectionUpdateAmount} <br />    
                      Advance Payment : {collectionUpdateAdvancePayment} <br /> 
                      Balance : {collectionUpdateBalance} <br />         
                      Commission : {collectionUpdateSalesCommission} <br />    
                      Credit Period : {collectionUpdateCreditPeriod} <br />                                            
                      
                      <br />
                      <h6>Invoice Date </h6>
                      {collectionUpdateInvoiceDate}

                      <br />
                      <br />
                      <h6>Due Date </h6>
                      {collectionUpdateDueDate}

                      <br/>
                      <br/>
                      <h6>Sales Representative </h6>
                      {collectionUpdateSalesRepName}
                      <br/>

                      <br/>
                      <hr/>

                      <div style={{ textAlign: "left"}}>
                        <Link
                          to="/collections"
                          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                          style={{ backgroundColor: "#fed136", width: "70%" }}
                        >
                          <Menu.Item name="Back" />
                        </Link>
                      </div>  

                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
              <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Update Collection Details</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      
                      {/* Client Name  */}
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Client Name
                      </Label>
                      <Form.Field className="form-control">
                          <select onChange={e =>
                            this.setState({ collectionUpdateClientID: e.target.value })
                          }>
                            <option key={collectionUpdateClientID} value={collectionUpdateClientName} selected>{collectionUpdateClientName}</option>
                              { clients.map(
                                      (client) => 
                                      <option key={client.id} 
                                          value={client.id} >{client.name}</option>) }                                                             
                          </select>                       
                      </Form.Field>
                      <br />

                      {/* Invoice Number              */}
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Invoice Number
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Invoice Number*"
                          className="form-control"
                          value={collectionUpdateInvoiceNumber}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateInvoiceNumber: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Invoice Date 
                      </Label>
                      <Form.Field className="form-control">
                        <DatePicker
                                selected={this.state.invoiceDate}
                                onChange={this.handleInvoiceDateChange}
                            />
                      </Form.Field>
                      <br />

    
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Due Date  
                      </Label>
                      <Form.Field className="form-control">
                        <DatePicker
                                selected={this.state.dueDate}
                                onChange={this.handleDueDateChange}
                          />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Amount
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Amount*"
                          className="form-control"
                          value={collectionUpdateAmount}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateAmount: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Credit Period
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Credit Period*"
                          className="form-control"
                          value={collectionUpdateCreditPeriod}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateCreditPeriod: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Advance Payment
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Advance Payment*"
                          className="form-control"
                          value={collectionUpdateAdvancePayment}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateAdvancePayment: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Balance
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Balance*"
                          className="form-control"
                          value={collectionUpdateBalance}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateBalance: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      {/* collectionUpdateSalesCommission */}
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Sales Commission
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Sales Commission*"
                          className="form-control"
                          value={collectionUpdateSalesCommission}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ collectionUpdateSalesCommission: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                         
                      {/* Sales Representative  */}                     
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Sales Representative
                      </Label>
                      <Form.Field className="form-control">
                      <select onChange={e =>
                            this.setState({ collectionUpdateSalesRepID: e.target.value })
                          }>
                            <option key={collectionUpdateSalesRepID} value={collectionUpdateSalesRepName} selected>{collectionUpdateSalesRepName}</option>
                              { salesRepresentatives.map(
                                      (salesRepresentative) => <option key={salesRepresentative.id} value={salesRepresentative.id}>{salesRepresentative.name}</option>) }
                          </select>                       
                      </Form.Field>
                      <br />

                      
                      <Row>
                        <div className="">
                          {button} &nbsp;                          
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default CollectionDetails;
