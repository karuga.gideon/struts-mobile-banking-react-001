import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import axios from "axios";

class UserDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "User Details";

    var userID = this.props.match.params.userID;
    console.log("userID >>> " + userID);

    this.state = {
      records: [],
      user_details: {},
      isLoading: false,
      pageOfItems: [],
      userID: userID,      
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    const { userID } = this.state;
    console.log("Getting User Details >>> " + userID);

    axios({
      url: `${API_BASE_URL}/users/` + userID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ 
              user_details: json_data, 
              userUpdateName: json_data.firstname + " " + json_data.lastname, 
              userUpdateFirstName: json_data.firstname, 
              userUpdateLastName: json_data.lastname, 
              userUpdatePhone: json_data.msisdn, 
              userUpdateEmail: json_data.email, 
              userUpdateUsername: json_data.username  });
          
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { user_details, userID } = this.state;

    var userPayLoad = {
        name: this.state.userUpdateName,
        firstname: this.state.userUpdateFirstName,
        lastname: this.state.userUpdateLastName,
        msisdn: this.state.userUpdatePhone,
        email: this.state.userUpdateEmail,
        username: this.state.userUpdateUsername
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + userID);
    console.log(user_details);

    axios({
      url: `${API_BASE_URL}/users/` + userID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("User Updated Successfully.");
          this.props.history.push("/users");
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    const { userID, user_details, userUpdateName, userUpdateFirstName, userUpdateLastName, userUpdatePhone, userUpdateEmail } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update User 
      </Form.Field>
    );
    
    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 
      
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <a href="dashboard">Admin</a>
            </li>
            <li className="breadcrumb-item active">Client Details</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <a className="btn" href="dashboard">
                  <i className="icon-speech"></i>
                </a>
                <a className="btn" href="dashboard">
                  <i className="icon-graph"></i>  Dashboard</a>
                <a className="btn" href="users">
                  <i className="icon-settings"></i>  Settings</a>
              </div>
            </li>
          </ol>
          <div className="container-fluid">

            <Row>              
              <Col md={8} xs={8}>
               <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Update User Details</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        First Name
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="First Name*"
                          className=""
                          value={userUpdateFirstName}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ userUpdateFirstName: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br /> 

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Last Name
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Last Name*"
                          className=""
                          value={userUpdateLastName}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ userUpdateLastName: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br /> 
                                           
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Phone Number
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Phone Number*"
                          className=""
                          value={userUpdatePhone}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ userUpdatePhone: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Email
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Email*"
                          className=""
                          value={userUpdateEmail}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ userUpdateEmail: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />                      

                      <Row>
                        <div className="">
                          {button} &nbsp;                          
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>

              <Col md={4} xs={12}>
                <Card className="card-user">   

                  <CardHeader>
                    <CardTitle>User Details</CardTitle>
                  </CardHeader>               

                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      
                      Name : {userUpdateName} <br />
                      Phone Number : {userUpdatePhone} <br />
                      Email Address: {userUpdateEmail} <br />                      
                      Date Created : {user_details.date_created} <br/>
                      User ID : {userID} <br />
                      
                      <hr/>

                      <div style={{ textAlign: "center"}}>
                        <Link
                          to="/users"
                          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                          style={{  width: "70%" }}
                        >
                          <Menu.Item name="Back" />
                        </Link>
                      </div>  
                    </p>
                    <div className="list-group">
                      <Link to="/about" className="list-group-item list-group-item-action">About</Link>
                      <Link to="/invoices" className="list-group-item list-group-item-action">Invoices </Link>
                      <Link to="/reports" className="list-group-item list-group-item-action">Reports</Link>
                      <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                      <Link to="/profile" className="list-group-item list-group-item-action active">Profile</Link>
                      <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                    </div>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default UserDetails;
