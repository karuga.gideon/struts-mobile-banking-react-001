import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { Form, Label } from "semantic-ui-react";
import axios from "axios";

class ClientDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Client Details";

    var clientID = this.props.match.params.clientID;
    console.log("clientID >>> " + clientID);

    this.state = {
      records: [],
      user_details: {},
      isLoading: false,
      pageOfItems: [],
      clientID: clientID,      
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    const { clientID } = this.state;
    console.log("Getting Client Details >>> " + clientID);

    axios({
      url: `${API_BASE_URL}/clients/` + clientID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ 
              user_details: json_data, 
              clientUpdateName: json_data.name, 
              clientUpdatePhone: json_data.phone, 
              clientUpdateEmail: json_data.email, 
              clientUpdateLocation: json_data.location  });
          
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { user_details, clientID } = this.state;

    var userPayLoad = {
        name: this.state.clientUpdateName,
        phone: this.state.clientUpdatePhone,
        email: this.state.clientUpdateEmail, 
        location: this.state.clientUpdateLocation 
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + clientID);
    console.log(user_details);

    axios({
      url: `${API_BASE_URL}/clients/` + clientID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("Client Updated Successfully.");
          this.props.history.push("/client-details/" + clientID);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    const { user_details, clientUpdateName, clientUpdatePhone, clientUpdateEmail, clientUpdateLocation } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update Client 
      </Form.Field>
    );
    
    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 
        
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <a href="dashboard">Admin</a>
            </li>
            <li className="breadcrumb-item active">Client Details</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <a className="btn" href="dashboard">
                  <i className="icon-speech"></i>
                </a>
                <a className="btn" href="dashboard">
                  <i className="icon-graph"></i>  Dashboard</a>
                <a className="btn" href="users">
                  <i className="icon-settings"></i>  Settings</a>
              </div>
            </li>
          </ol>
          <div className="container-fluid">

            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">                  
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> Client Details </h6>
                      Name : {clientUpdateName} <br />
                      Phone Number : {clientUpdatePhone} <br />
                      Email Address: {clientUpdateEmail} <br />    
                      Location : {clientUpdateLocation} <br />                      
                      <br />
                      <h6>Date Created </h6>
                      {user_details.date_created}

                      <br/>
                      <hr/>

                      <div style={{ textAlign: "left"}}>
                        <Link
                          to="/clients"
                          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                          style={{ backgroundColor: "#fed136", width: "70%" }}
                        >
                          <Menu.Item name="Back" />
                        </Link>
                      </div>  
                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
              <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Update Client Details</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Name
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Name*"
                          className="form-control"
                          value={clientUpdateName}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ clientUpdateName: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />                      
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Phone Number
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Phone Number*"
                          className="form-control"
                          value={clientUpdatePhone}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ clientUpdatePhone: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Email
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Email*"
                          className="form-control"
                          value={clientUpdateEmail}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ clientUpdateEmail: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Location
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Location*"
                          className="form-control"
                          value={clientUpdateLocation}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ clientUpdateLocation: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      
                      <Row>
                        <div className="">
                          {button} &nbsp;                          
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default ClientDetails;
