import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";

import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import axios from "axios";

class SalesRepDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Sales Rep Details";

    var salesRepID = this.props.match.params.salesRepID;
    console.log("salesRepID >>> " + salesRepID);

    this.state = {
      records: [],
      user_details: {},
      isLoading: false,
      pageOfItems: [],
      salesRepID: salesRepID,      
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    const { salesRepID } = this.state;
    console.log("Getting Sales Rep Details >>> " + salesRepID);

    axios({
      url: `${API_BASE_URL}/salesrepresentatives/` + salesRepID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        

        if (!response.data.error) {
          this.setState({ 
              user_details: json_data, 
              salesRepUpdateName: json_data.name, 
              salesRepUpdatePhone: json_data.phone, 
              salesRepUpdateEmail: json_data.email, 
              salesRepUpdateUsername: json_data.username  });
          
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { user_details, salesRepID } = this.state;

    var userPayLoad = {
        name: this.state.salesRepUpdateName,
        phone: this.state.salesRepUpdatePhone,
        email: this.state.salesRepUpdateEmail
    };

    var loggedIn = getUserDetails();
    
    
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + salesRepID);
    console.log(user_details);

    axios({
      url: `${API_BASE_URL}/salesrepresentatives/` + salesRepID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          alert("Sales Rep Updated Successfully.");
          this.props.history.push("/sales-rep/" + salesRepID);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    const { user_details, salesRepUpdateName, salesRepUpdatePhone, salesRepUpdateEmail } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update Sales Rep 
      </Form.Field>
    );
    
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">                  
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> Sales Representative Details </h6>
                      Name : {salesRepUpdateName} <br />
                      Phone Number : {salesRepUpdatePhone} <br />
                      Email Address: {salesRepUpdateEmail} <br />                      
                      <br />
                      <h6>Date Created </h6>
                      {user_details.date_created}

                      <br/>
                      <br/>
                      <hr/>
                      <div style={{ textAlign: "left"}}>
                        <Link
                          to="/sales_reps"
                          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                          style={{ backgroundColor: "#fed136", width: "70%" }}
                        >
                          <Menu.Item name="Back" />
                        </Link>
                      </div>  

                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
              <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Update User Details</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Name
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Name*"
                          className="form-control"
                          value={salesRepUpdateName}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ salesRepUpdateName: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />                      
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Phone Number
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Phone Number*"
                          className="form-control"
                          value={salesRepUpdatePhone}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ salesRepUpdatePhone: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Email
                      </Label>
                      <Form.Field className="form-control">
                        <input
                          type="text"
                          placeholder="Email*"
                          className="form-control"
                          value={salesRepUpdateEmail}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ salesRepUpdateEmail: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      
                      <Row>
                        <div className="">
                          {button} &nbsp;                          
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default SalesRepDetails;
