import React from "react";

import { NavLink, Link } from "react-router-dom";
import { FaHome, FaDatabase, FaUsers, FaUser, FaInfo, FaAddressCard, FaBuilding } from 'react-icons/fa';

import userAvatar from '../../assets/img/avatars/user_avatar.png';

export function loadHeader() {

  return (
    <header className="app-header navbar">
      <button className="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span className="navbar-toggler-icon"></span>
      </button>

      <Link className="navbar-brand" to="/dashboard">
        Mobile Banking
                </Link>

      <button className="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span className="navbar-toggler-icon"></span>
      </button>
      <ul className="nav navbar-nav d-md-down-none">
        <li className="nav-item px-3">
          <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
        </li>
        <li className="nav-item px-3">
          <Link className="nav-link" to="users">Users</Link>
        </li>
        <li className="nav-item px-3">
          <Link className="nav-link" to="/dashboard">Settings</Link>
        </li>
      </ul>
      <ul className="nav navbar-nav ml-auto">
        {/* <li className="nav-item d-md-down-none">
                    <a className="nav-link" href="clients">
                        <i className="icon-bell"></i>
                        <span className="badge badge-pill badge-danger">5</span>
                    </a>
                    </li> */}
        <li className="nav-item d-md-down-none">
          <a className="nav-link" href="clients">
            <i className="icon-list"></i>
          </a>
        </li>
        <li className="nav-item d-md-down-none">
          <a className="nav-link" href="clients">
            <i className="icon-location-pin"></i>
          </a>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link" data-toggle="dropdown" href="/dashboard" role="button" aria-haspopup="true" aria-expanded="false">
            <img className="img-avatar" src={userAvatar} alt="Account" />
          </a>
          <div className="dropdown-menu dropdown-menu-right">
            <div className="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <Link className="dropdown-item" to="dashboard">
              <i className="fa fa-bell-o"></i> Dashboard
                          <span className="badge badge-info">4</span>
            </Link>
            <Link className="dropdown-item" to="invoices">
              <i className="fa fa-envelope-o"></i> Invoices
                          <span className="badge badge-success">42</span>
            </Link>
            <Link className="dropdown-item" to="reports">
              <i className="fa fa-tasks"></i> Reports
                          <span className="badge badge-danger">42</span>
            </Link>
            <Link className="dropdown-item" to="users">
              <i className="fa fa-comments"></i> Users
                          <span className="badge badge-warning">42</span>
            </Link>
            <div className="dropdown-header text-center">
              <strong>Settings</strong>
            </div>
            <Link className="dropdown-item" to="profile">
              <i className="fa fa-user"></i> Profile
                        </Link>
            <Link className="dropdown-item" to="profile">
              <i className="fa fa-wrench"></i> Settings
                        </Link>
            {/* <Link className="dropdown-item" to="clients">
                        <i className="fa fa-usd"></i> Payments
                        <span className="badge badge-secondary">42</span>
                        </Link> */}
            {/* <Link className="dropdown-item" to="clients">
                        <i className="fa fa-file"></i> Projects
                        <span className="badge badge-primary">42</span>
                        </Link> */}
            {/* <div className="dropdown-divider"></div> */}
            {/* <Link className="dropdown-item" to="clients">
                        <i className="fa fa-shield"></i> Lock Account</Link> */}
            <Link className="dropdown-item" to="/">
              <i className="fa fa-lock"></i> Logout
                        </Link>
          </div>
        </li>
      </ul>
      {/* <button className="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <button className="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
                    <span className="navbar-toggler-icon"></span>
                </button> */}
    </header>
  );
}


export function loadSidebarMenu(currentPage) {

  var activeItem = window.location.pathname;
  var isActive = activeItem === currentPage;
  var activeClassName = isActive ? 'nav-link' : 'nav-link';

  return (
    <div className="sidebar">
      <nav className="sidebar-nav">
        {/* <div className="sidebar-header">
            <h3>Bootstrap Sidebar</h3>
        </div> */}

        <ul className="nav">
          {/* <p>Dummy Heading</p> */}

          <li className="nav-item">
            <NavLink className="nav-link" to="/dashboard">
              <i className="nav-icon icon-speedometer"><FaHome /></i> Dashboard
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className="nav-link" to="/customers">
              <i className="nav-icon icon-people"><FaAddressCard /></i> Customers
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className="nav-link" to="/transactions">
              <i className="nav-icon icon-people"><FaAddressCard /></i> Transactions
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" className="nav-link nav-dropdown-toggle">
              <i className="nav-icon icon-puzzle"><FaBuilding /></i> Reports</a>
            <ul className="collapse list-unstyled" id="pageSubmenu">
              <li className="nav-item" >
                <NavLink className="nav-link" to="/reports">
                  &nbsp; &nbsp; <FaBuilding /> &nbsp; Reports
                      </NavLink>
              </li>
              <li className="nav-item" >
                <NavLink className="nav-link" to="/transactions">
                  &nbsp; &nbsp; <FaBuilding /> &nbsp; Transactions
                      </NavLink>
              </li>
            </ul>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/users">
              <i className="nav-icon icon-puzzle"><FaUsers /></i> Users
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/profile">
              <i className="nav-icon icon-speedometer"><FaUser /></i> Profile
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/about">
              <i className="nav-icon icon-speedometer"><FaInfo /></i> About
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="divider"></li>

        </ul>
      </nav>

    </div>
  );
}

export function loadSidebarMenu_001(currentPage) {

  var activeItem = window.location.pathname;
  var isActive = activeItem === currentPage;
  var activeClassName = isActive ? 'nav-link' : 'nav-link';

  return (
    <div className="sidebar">
      <nav className="sidebar-nav">
        <ul className="nav">
          <li className="nav-item">
            <NavLink className="nav-link" to="/dashboard">
              <i className="nav-icon icon-speedometer"><FaHome /></i> Dashboard
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>
          <li className="nav-item nav-dropdown">
            <NavLink className="nav-link nav-dropdown-toggle" to="/users">
              <i className="nav-icon icon-puzzle"><FaBuilding /></i> Institutions</NavLink>
            <ul className="nav-dropdown-items">
              <li className="nav-item">
                <NavLink className="nav-link" to="/users">
                  &nbsp; &nbsp; <FaBuilding /> &nbsp; Users
                  </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/reports">
                  &nbsp; &nbsp; <FaBuilding /> &nbsp; Reports
                  </NavLink>
              </li>
            </ul>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/invoices">
              <i className="nav-icon icon-puzzle"><FaAddressCard /></i> Invoices
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/reports">
              <i className="nav-icon icon-puzzle"><FaDatabase /></i> Reports
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/users">
              <i className="nav-icon icon-puzzle"><FaUsers /></i> Users
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          {/* <li className="nav-item nav-dropdown">
              <Link className="nav-link nav-dropdown-toggle" to="/institutions">
                <i className="nav-icon icon-puzzle"><FaCogs /></i> Settings
              </Link>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <Link  className="nav-link" to="/institutions">
                    &nbsp; &nbsp; <FaCogs /> &nbsp; User Types
                  </Link>
                </li>
                <li className="nav-item">
                  <Link  className="nav-link" to="/institutiontypes">
                  &nbsp; &nbsp; <FaCogs /> &nbsp; Permissions 
                  </Link>
                </li>                
              </ul>
            </li> */}

          <li className="nav-item">
            <NavLink className={activeClassName} to="/profile">
              <i className="nav-icon icon-speedometer"><FaUser /></i> Profile
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className={activeClassName} to="/about">
              <i className="nav-icon icon-speedometer"><FaInfo /></i> About
                <span className="badge badge-primary"></span>
            </NavLink>
          </li>

          <li className="divider"></li>

        </ul>
      </nav>
      {/* <button className="sidebar-minimizer brand-minimizer" type="button"></button> */}
    </div>
  );
}

export function formatAmount(n, currency) {
  return currency + n.toFixed(2).replace(/./g, function (c, i, a) {
    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
  });
}