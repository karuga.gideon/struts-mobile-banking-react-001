import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Menu,
  Responsive,
  Visibility
} from "semantic-ui-react";
import "../../App.css";

export default class Navbar extends Component {
  state = {};

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {

    return (
      <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <nav
            className="navbar navbar-expand-lg navbar-dark fixed-top"
            id="mainNav"
            style={{ backgroundColor: "#2a292e", height: "5%" }}
          >
            <div className="container">
              <a className="navbar-brand js-scroll-trigger" href="/">
                Struts Credit Control
              </a>
              {/* <Link className="navbar-brand js-scroll-trigger" to="/">
                Credit Control
              </Link> */}
              <button
                className="navbar-toggler navbar-toggler-right"
                type="button"
                data-toggle="collapse"
                data-target="#navbarResponsive"
                aria-controls="navbarResponsive"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                Menu
                <i className="fas fa-bars" />
              </button>
              <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav text-uppercase ml-auto">
                  {/* <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#services">
                      Services
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#portfolio">
                      Portfolio
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#about">
                      About
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#team">
                      Team
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#contact">
                      Contact
                    </a>
                  </li> */}
                  <li className="nav-item">
                    <Link to="/signin" className="nav-link js-scroll-trigger">
                      <Menu.Item name="Sign In" />
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </Visibility>
      </Responsive>
    );
  }
}
