import React from "react";

export default () => (
  <div className="spinner">
    <div className="dot1" />
    <div className="dot2" />
  </div>
);
