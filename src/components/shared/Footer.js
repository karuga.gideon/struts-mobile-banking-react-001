import React, { Component } from "react";
import "../../App.css";

export default class Footer extends Component {

  state = {};

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {  
    
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <span className="copyright">Copyright © Struts Credit Control 2019</span>
            </div>
            <div className="col-md-4">
              <ul className="list-inline social-buttons">
                <li className="list-inline-item">
                  <a href="http://www.strutstechnology.co.ke" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-twitter" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="http://www.strutstechnology.co.ke" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-facebook-f" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="http://www.strutstechnology.co.ke" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-4">
              <ul className="list-inline quicklinks">
                <li className="list-inline-item">
                  <a href="http://www.strutstechnology.co.ke" target="_blank" rel="noopener noreferrer">Privacy Policy</a>
                </li>
                <li className="list-inline-item">
                <a href="http://www.strutstechnology.co.ke" target="_blank" rel="noopener noreferrer">Terms of Use</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
