import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Apps 
import SignIn from "./components/pages/SignIn";
import SignUp from "./components/pages/SignUp";
import HomePage from "./components/pages/HomePage";
import Dashboard from "./components/pages/app/Dashboard";
import Invoices from "./components/pages/app/Invoices";
import Collections from "./components/pages/app/Collections";
import Clients from "./components/pages/app/Clients";
import Customers from "./components/pages/app/Customers";
import Institutions from "./components/pages/app/Institutions";
import InstitutionTypes from "./components/pages/app/InstitutionTypes";
import Students from "./components/pages/app/Students";
import Transactions from "./components/pages/app/Transactions";
import SalesRepresentatives from "./components/pages/app/SalesRepresentatives";
import Users from "./components/pages/app/Users";
import TopUp from "./components/pages/app/TopUp";
import Reports from "./components/pages/app/Reports";
import Profile from "./components/pages/app/Profile";
import About from "./components/pages/app/About";

// Forms
import CreateClient from "./components/pages/forms/create_client";
import CreateTransaction from "./components/pages/forms/create_transaction";
import CreateSalesRepresentative from "./components/pages/forms/create_sales_representative";
import CreateUser from "./components/pages/forms/create_user";
import CreateSurvey from "./components/pages/forms/create_survey";
import CreateCollection from "./components/pages/forms/create_collection";
import ChangePassword from "./components/pages/forms/ChangePassword";
import UploadCustomers from "./components/pages/forms/upload_customers";

// View
import ClientDetails from "./components/pages/view/ClientDetails";
import UserDetails from "./components/pages/view/UserDetails";
import SalesRepDetails from "./components/pages/view/SalesRepDetails";
import CollectionDetails from "./components/pages/view/CollectionDetails";
import InvoiceDetails from "./components/pages/view/InvoiceDetails";


class App extends React.Component {
  render() {
    return (
      <div>
        <Router className="App">
          <React.Fragment>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/signin" component={SignIn} />
              <Route path="/signup" component={SignUp} />

              {/* Apps */}
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/invoices" component={Invoices} />
              <Route path="/clients" component={Clients} />
              <Route path="/customers" component={Customers} />
              <Route path="/institutions" component={Institutions} />
              <Route path="/institutiontypes" component={InstitutionTypes} />
              <Route path="/students" component={Students} />
              <Route path="/transactions" component={Transactions} />
              <Route path="/sales_reps" component={SalesRepresentatives} />
              <Route path="/collections" component={Collections} />
              <Route path="/topup" component={TopUp} />
              <Route path="/reports" component={Reports} />
              <Route path="/users" component={Users} />
              <Route path="/profile" component={Profile} />
              <Route path="/about" component={About} />
              <Route path="/change-password" component={ChangePassword} />


              {/* Forms  */}
              <Route path="/create-survey" component={CreateSurvey} />
              <Route path="/create-client" component={CreateClient} />
              <Route path="/create-transaction" component={CreateTransaction} />
              <Route path="/create-user" component={CreateUser} />
              <Route path="/create-sales-rep" component={CreateSalesRepresentative} />
              <Route path="/create-collection" component={CreateCollection} />
              <Route path="/upload-customers" component={UploadCustomers} />



              {/* Views */}
              <Route path="/client-details/:clientID" component={ClientDetails} />
              <Route path="/user/:userID" component={UserDetails} />
              <Route path="/sales-rep/:salesRepID" component={SalesRepDetails} />
              <Route path="/collection-details/:collectionID" component={CollectionDetails} />
              <Route path="/invoice/:invoiceID" component={InvoiceDetails} />


              {/* <Route path="*" component={NotFoundPage} /> */}
              <Route path="*" component={HomePage} />
            </Switch>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

// Admin theme downloaded from
// https://demos.creative-tim.com/paper-dashboard-react/#/dashboard
